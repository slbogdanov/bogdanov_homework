#include <iostream>
#include <cstdlib>
#include <string.h>
#include <string>
#include <stdio.h>
#include <ostream>
#include <assert.h>


class my_string
{
public:
    std::istream& operator >> (std::istream& stream)
    {
        char symb;
        while(stream.get(symb) && (symb == '\n' || symb == '\r' || symb == '\t'));
        do
        {
            this -> insert_character(this->str_size - 1, symb);
        }
        while(stream.get(symb) && symb != '\n' && symb != '\r' && symb != '\t');
        return stream;
    }
    void deleteLast()
    {
        this -> delete_characters(str_size - 1, 1);
    }
    void append(char ch)
    {
        this -> insert_character(0, ch);
    }
    void delete_characters(int first_char, int lenght)
    {
        assert(str_size >= lenght);
        str_size -= lenght;
        for(int i = first_char; i < str_size; ++i)
        {
            str[i] = str[i + lenght];
        }
        if(str_size < str_capacity/4)
        {
            smart_capacity_decrease();
        }
    }
    void insert_character(int index, char ch)
    {
        assert(index < str_capacity);
        if(str_size + 1 > str_capacity)
        {
            this -> smart_capacity_increase();
        }
        for(int i = str_size; i > index; --i)
        {
            str[i] = str[i - 1];
        }
        str[index] = ch;
        str_size += 1;
    }
    void smart_capacity_decrease ()
    {
        assert(str_capacity > 1);
        str_capacity /= 2;
        char* new_str;
        new_str = new char[str_capacity*sizeof(char)];
        memcpy(new_str, str, str_size);
        delete[] str;
        str = new_str;
    }
    void smart_capacity_increase ()
    {
        str_capacity *= 2;
        char* new_str;
        new_str = new char[str_capacity*sizeof(char)];
        memcpy(new_str, str, str_size);
        delete[] str;
        str = new_str;
    }
    const char* c_str() const
    {
        return str;
    }
    std::ostream& operator << (std::ostream& stream)
    {
        stream << str;
        return stream;
    }
    my_string& operator+= (const my_string& second_str)
    {
        char* temp_str;
        temp_str = new char[(str_size + second_str.size()) * sizeof(char)];
        int i;
        for(i = 0; i < str_size - 1; ++i)
        {
            temp_str[i] = str[i];
        }
        for(int j = 0; j < second_str.size() + 1; ++j)
        {
            temp_str[i + j] = second_str[j];
        }
        *this = temp_str;
        return *this;
    }
    char& operator[] (int position)
    {
        return str[position];
    }
    char& operator[] (int position) const
    {
        return str[position];
    }
    my_string(const my_string& given_str)
    {
        str_size = given_str.size();
        str_capacity = 1;
        while(str_capacity < str_size)
        {
            str_capacity *= 2;
        }
        str = new char[str_capacity * sizeof(char)];
        for(int i = 0; i < str_size; ++i)
        {
            str[i] = given_str[i];
        }
    }
    my_string(const std::string& given_str)
    {
        str_size = given_str.size();
        str_capacity = 1;
        while(str_capacity < str_size + 1)
        {
            str_capacity *= 2;
        }
        str = new char[str_capacity * sizeof(char)];
        for(int i = 0; i < str_size + 1; ++i)
        {
            str[i] = given_str[i];
        }
    }
    my_string(const char* given_str)
    {
        int str_len = 1;
        for(int i = 0; given_str[i] != '\0'; ++i)
        {
            ++str_len;
        }
        str_size = str_len;
        str_capacity = 1;
        while(str_capacity < str_size)
        {
            str_capacity *= 2;
        }
        delete[] str;
        str = new char[str_capacity * sizeof(char)];
        for(int i = 0; i < str_size; ++i)
        {
            str[i] = given_str[i];
        }
    }
    my_string()
    {
        str = new char[sizeof(char)];
        str_capacity = 1;
        str_size = 1;
        str[0] = '\0';
    }
    ~my_string()
    {
        delete[] str;
    }
    my_string& operator= (const std::string& given_str)
    {
        delete [] str;
        str_size = given_str.size();
        str_capacity = 1;
        while(str_capacity < str_size)
        {
            str_capacity *= 2;
        }
        str = new char[str_capacity * sizeof(char)];
        for(int i = 0; i < str_size; ++i)
        {
            str[i] = given_str[i];
        }
        return *this;
    }
    my_string& operator= (const my_string& given_str)
    {
        delete [] str;
        str_size = given_str.size();
        str_capacity = 1;
        while(str_capacity < str_size)
        {
            str_capacity *= 2;
        }
        str = new char[str_capacity * sizeof(char)];
        for(int i = 0; i < str_size; ++i)
        {
            str[i] = given_str[i];
        }
        return *this;
    }
    my_string& operator= (const char* given_str)
    {
        delete [] str;
        int str_len = 1;
        for(int i = 0; given_str[i] != '\0'; ++i)
        {
            ++str_len;
        }
        str_size = str_len;
        str_capacity = 1;
        while(str_capacity < str_size)
        {
            str_capacity *= 2;
        }
        str = new char[str_capacity * sizeof(char)];
        for(int i = 0; i < str_size; ++i)
        {
            str[i] = given_str[i];
        }
        return *this;
    }
    bool operator< (const my_string& given_str)
    {
        int i;
        for(i = 0; i < str_size && i < given_str.size(); ++i)
        {
            if(str[i] < given_str[i])
            {
                return 1;
            }
            if(str[i] > given_str[i])
            {
                return 0;
            }
        }
        if(i == given_str.size())
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }
    int size()
    {
        return str_size;
    }
    int capacity()
    {
        return str_capacity;
    }
    int size() const
    {
        return str_size;
    }
    int capacity() const
    {
        return str_capacity;
    }
private:
    char* str;
    int str_capacity;
    int str_size;
};


int main()
{
    char* testchararr;
    testchararr = new char[100 * sizeof(char)];
    std::cout << "enter a string (max length 100 symbols, min length 7 symbols) >\n";
    std::cin >> testchararr;
    my_string mystring(testchararr);
    std::cout << "casted string :\n";
    mystring << std::cout << "\n";
    std::string teststring;
    std::cout << "enter a string of std::string type:\n";
    std::cin >> teststring;
    my_string myanotherstring(teststring);
    std::cout << "casted string :\n";
    myanotherstring << std::cout << "\n";
    std::cout << "first and second string concatenated:\n";
    (mystring += myanotherstring) << std::cout << "\n";
    mystring[6] = 'z';
    std::cout << "changed changed 7'th symbol to z\n";
     mystring << std::cout << "\n";
    mystring.insert_character(0, '0');
    mystring.insert_character(4, 'k');
    mystring.delete_characters(1, 3);
    std::cout << "inserted 0 as first symbol, k as fifth symbol, deleted symbols from 2 to 4:\n";
    mystring << std::cout << "\n";
    myanotherstring = mystring;
    std::cout << "copied string :\n";
    myanotherstring << std::cout << "\n";
    std::cout << "enter a string:\n";
    my_string mylaststring;
    mylaststring >> std::cin;
    std::cout << "string scanned from std stream :\n";
    mylaststring << std::cout << "\n";
    delete[] testchararr;
    return 0;
}
