#include <iostream>
#include <string.h>

class Human
{
public:
    virtual std::string GetName() {return 0;}
    Human (const std::string& a, const std::string& b): firstname(a), lastname(b) {}
    virtual ~Human() = 0;
protected:
    std::string firstname;
    std::string lastname;
};
Human::~Human() {}

class Mister : public Human
{
public:
    std::string GetName()
    {
        return "Mr. " + lastname;
    }
    Mister(const std::string& a, const std::string&b): Human(a, b){};
    ~Mister()
    {
        firstname.clear();
        lastname.clear();
    }
};

class Miss : public Human
{
public:
    std::string GetName()
    {
        return "Ms." + lastname;
    }
    Miss(const std::string& a, const std::string&b): Human(a, b){};
    ~Miss()
    {
        firstname.clear();
        lastname.clear();
    }
};

class Kid : public Human
{
public:
    std::string GetName()
    {
        return firstname;
    }
    Kid(const std::string& a, const std::string&b): Human(a, b){};
    ~Kid()
    {
        firstname.clear();
        lastname.clear();
    }
};

int main()
{
    std::string mystr1, mystr2;
    std::cin >> mystr1 >> mystr2;
    Human* mister_a = new Mister(mystr1, mystr2);
    std::cin >> mystr1 >> mystr2;
    Human* miss_b = new Miss(mystr1, mystr2);
    std::cin >> mystr1 >> mystr2;
    Human* kid_c = new Kid(mystr1, mystr2);
    std::cout << mister_a -> GetName() << " \n" << miss_b -> GetName() << " \n" << kid_c -> GetName() << "\n";
    delete mister_a;
    delete miss_b;
    delete kid_c;
    return 0;
}
