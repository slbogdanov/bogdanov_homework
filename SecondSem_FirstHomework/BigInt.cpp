#include <fstream>
#include <vector>
#define THRESHOLD 10000
#include <iomanip>
#include <iostream>
#include <cmath>
#include <limits>
#include <limits.h>

class BigInt
{
public:
    BigInt() {sign = 0;}
    ~BigInt()
    {
        parts.clear();
    }
    BigInt(int a)
    {
        parts.push_back(a % THRESHOLD);
        parts.push_back((a / THRESHOLD) % THRESHOLD);
        parts.push_back(a / (THRESHOLD * THRESHOLD));
        sign = 0;
    }
    BigInt operator* (const BigInt& another_bigint)
    {
        int extra = 0;
        BigInt res_num;
        int res_size;
        res_size = parts.size() + another_bigint.parts.size();
        res_num.parts.resize(res_size, 0);
        for(int i = 0; i < parts.size(); ++i)
        {
            for(int j = 0; j < another_bigint.parts.size(); ++j)
            {
                res_num.parts[i + j] += parts[i] * another_bigint.parts[j];
                if(res_num.parts[i + j] > THRESHOLD * THRESHOLD)
                {
                    extra = res_num.parts[i + j] / (THRESHOLD * THRESHOLD);
                    res_num.parts[i + j] -= extra * (THRESHOLD * THRESHOLD);
                    res_num.parts[i + j + 2] += extra;
                    extra = 0;
                }
            }
        }
        for(int i = 0; i < res_size; ++i)
        {
            res_num.parts[i] += extra;
            extra = res_num.parts[i] / THRESHOLD;
            res_num.parts[i] -= extra * THRESHOLD;
        }
        return res_num;
    }
    BigInt operator% (const BigInt& another_bigint)
    {
        long long residue = 0;
        int divider = 0;
        int thresholds_power = 1;
        for(int i = 0; i < another_bigint.parts.size(); ++i)
        {
            divider += another_bigint.parts[i] * thresholds_power;
            thresholds_power *= THRESHOLD;
        }
        for (int i = parts.size() - 1; i >= 0; --i)
        {
            residue += parts[i];
            residue %= divider;
            residue *= THRESHOLD;
        }
        divider = residue / THRESHOLD;
        BigInt res_num (divider);
        return res_num;
    }
    BigInt operator/ (const BigInt& another_bigint)
    {
        int div;
        int extra = 0;
        BigInt res_num;
        res_num.parts.resize(parts.size(), 0);
        for(unsigned int i = parts.size(); i > 0 ; --i)
        {
            div = parts[i - 1] + extra * THRESHOLD;
            res_num.parts[i - 1] = (div / another_bigint.parts[0]);
            extra = div % another_bigint.parts[0];
        }
        return res_num;
    }
    BigInt operator* (const int& another_bigint)
    {
        int mult;
        int extra = 0;
        BigInt res_num;
        for(unsigned int i = 0; (i < parts.size()) || (extra > 0); ++i)
        {
            mult = ((parts[i] * (i < parts.size())) * another_bigint + extra);
            res_num.parts.push_back(mult % THRESHOLD);
            extra = mult / THRESHOLD;
        }
        return res_num;
    }
    BigInt operator- (const BigInt& another_bigint) const
    {
        signed int extra = 0;
        BigInt res_num;
        int res_size;
        res_size = std::max(parts.size(), another_bigint.parts.size());
        signed int i;
        int first_number;
        int second_number;
        for(i = res_size; i > 0; --i)
        {
            if(parts[i - 1]* ((i-1) < parts.size()) < another_bigint.parts[i - 1] * ((i-1) < another_bigint.parts.size()))
            {
                res_num = (another_bigint - *this);
                res_num.sign = 1;
                return res_num;
            }
            if(parts[i - 1]* ((i-1) < parts.size()) > another_bigint.parts[i - 1] * ((i-1) < another_bigint.parts.size()))
            {
                break;
            }
        }
        if(i == 0)
        {
            res_num.parts.push_back(0);
            return res_num;
        }
        res_num.parts.resize(res_size, 0);
        for(i = 0; i < res_size; ++i)
        {
            first_number = parts[i] * ((i) < parts.size());
            second_number = (another_bigint.parts[i] * ((i) < another_bigint.parts.size())) + extra;
            if (second_number > first_number)
            {
                first_number += THRESHOLD;
                extra = 1;
            }
            else
            {
                extra = 0;
            }
            res_num.parts[i] = first_number - second_number;
        }
        if(res_num.parts.size() == 0)
        {
            res_num.parts.push_back(0);
        }
        return res_num;
    }
    BigInt operator+ (const BigInt& another_bigint)
    {
        int sum;
        int extra = 0;
        BigInt res_num;
        for(unsigned int i = 0; (i < parts.size()) || (i < another_bigint.parts.size()) || (extra > 0); ++i)
        {
            sum = ((parts[i] * (i < parts.size())) + (another_bigint.parts[i] * (i < another_bigint.parts.size())) + extra);
            res_num.parts.push_back(sum % THRESHOLD);
            extra = sum / THRESHOLD;
        }
        return res_num;
    }
    BigInt& operator= (const BigInt& another_bigint)
    {
        this -> parts.clear();
        this -> sign = another_bigint.sign;
        for(auto it = another_bigint.parts.begin(); it != another_bigint.parts.end(); ++it)
        {
            parts.push_back(*it);
        }
        return *this;
    }
    std::istream& operator>> (std::istream& input)
    {
        std::string mystring;
        int power = 1;
        int number = 0;
        input >> mystring;
        for(signed int i = mystring.size(); i > 0; --i)
        {
            number += (mystring[i - 1] - '0') * power;
            power *= 10;
            if(power == THRESHOLD)
            {
                parts.push_back(number);
                number = 0;
                power = 1;
            }
        }
        if(number != 0 || parts.size() == 0)
        {
            parts.push_back(number);
        }
        return input;
    }
    std::ostream& operator<< (std::ostream& output)
    {
        if(sign == 1)
        {
            output << "-";
        }
        auto it = (--parts.end());
        while((*it) == 0 && it != parts.begin())
        {
            --it;
        }
        output << (*it);
        if(it != parts.begin())
        {
            --it;
            for(; it != parts.begin(); --it)
            {
                output << std::setfill('0') << std::setw(log10(THRESHOLD)) << (*it);
            }
            output << std::setfill('0') << std::setw(log10(THRESHOLD)) << (*parts.begin());
        }
        return output;
    }
    std::vector<int> parts;
    bool sign;
};

int main()
{
    std::ofstream output;
    std::ifstream input;
    std::string mystring;
    input.open("INPUT.TXT");
    output.open("OUTPUT.TXT");
    BigInt a;
    a >> input;
    BigInt b;
    b >> input;
    BigInt c;
    c = (a % b);
    c << output;
    input.close();
    output.close();
    return 0;
}
