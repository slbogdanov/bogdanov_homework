#include <iostream>
#include <string>
#include <vector>
#include <string>
#include <assert.h>

class World;
void launch();

class GameObject
{
protected:
    friend World;
    friend void launch();
    void go_move(int new_x, int new_y)
    {
        x = new_x;
        y = new_y;
    }
    virtual char symbol() const {return '?';}
    virtual void print() const {}
    virtual void self_render(std::string& wmap, int height, int width){}
    GameObject(int& new_x, int& new_y, int new_go_size)
    {
        x = new_x;
        y = new_y;
        go_size = new_go_size;
    }
    virtual ~GameObject(){}
    int x;
    int y;
    int go_size;
};


class Creature : public GameObject
{
protected:
    friend World;
    friend void launch();
    virtual char symbol() const {return '?';}
    virtual void print() const{}
    virtual void self_render(std::string& wmap, int height, int width){}
    Creature(int& x, int& y, int& hp): GameObject(x, y, 1)
    {
        hitpoints = hp;
    }
    int hitpoints;
};

class Hero : public Creature
{
protected:
    friend World;
    friend void launch();
    char symbol() const {return name[0];}
    void print() const {std::cout << "Legendary hero " << name <<" with hp " << hitpoints <<" at ("<< x <<", " << y << ") \n";}
    void self_render(std::string& wmap, int height, int width)
    {
        assert(wmap[(y - 1) * (width * 2 + 1) + (x - 1) * 2] == '.');
        wmap[(y - 1) * (width * 2 + 1) + (x - 1) * 2] = this -> symbol();
    }
    Hero(int x, int y, int hp, std::string name): Creature(x, y, hp), name(name){}
    std::string name;
};

class Monster : public Creature
{
protected:
    friend World;
    friend void launch();
    char symbol() const {return 'm';}
    void print() const {std::cout << "Scary monster with hp  "<< hitpoints <<" at ("<< x <<", " << y << ") \n";}
    void self_render(std::string& wmap, int height, int width)
    {
        assert(wmap[(y - 1) * (width * 2 + 1) + (x - 1) * 2] == '.');
        wmap[(y - 1) * (width * 2 + 1) + (x - 1) * 2] = this -> symbol();
    }
    Monster(int x, int y, int hp): Creature(x, y, hp){}
};

class Terrain : public GameObject
{
protected:
    friend World;
    friend void launch();
    virtual char symbol() const {return '?';}
    virtual void print() const{}
    virtual void self_render(std::string& wmap, int height, int width){}
    Terrain(int x, int y, int go_size): GameObject(x, y, go_size){}
};

class Lake : public Terrain
{
protected:
    friend World;
    friend void launch();
    char symbol() const {return 'o';}
    void print() const {std::cout << "Beautiful lake of " << depth <<" depth at ("<< x <<", " << y << ") " << go_size <<" size\n";}
    void self_render(std::string& wmap, int height, int width)
    {
        for (int i = 0; i < go_size; ++i)
        {
            for (int j = 0; j < go_size; ++j)
            {
                assert(wmap[(y - 1 + i) * (width * 2 + 1) + (x - 1 + j) * 2] == '.');
                wmap[(y - 1 + i) * (width * 2 + 1) + (x - 1 + j) * 2] = this -> symbol();
            }
        }
    }
    Lake(int x, int y, int go_size, int depth): Terrain(x, y, go_size), depth(depth){}
    int depth;
};

class Mountain : public Terrain
{
protected:
    friend World;
    friend void launch();
    char symbol() const {return '^';}
    void print() const {std::cout << "Ominous mountain " << height <<" high at ("<< x <<", " << y << ") " << go_size <<" size\n";}
    Mountain(int x, int y, int go_size, int height): Terrain(x, y, go_size), height(height){}
    void self_render(std::string& wmap, int height, int width)
    {
        for (int i = 0; i < go_size; ++i)
        {
            for (int j = 0; j < go_size; ++j)
            {
                assert(wmap[(y - 1 + i) * (width * 2 + 1) + (x - 1 + j) * 2] == '.');
                wmap[(y - 1 + i) * (width * 2 + 1) + (x - 1 + j) * 2] = this -> symbol();
            }
        }
    }
    int height;
};

class Forest : public Terrain
{
protected:
    friend World;
    friend void launch();
    char symbol() const {return 'F';}
    void print() const {std::cout << "Dark forest at ("<< x <<", " << y << ") " << go_size <<" size\n";}
    Forest(int x, int y, int go_size): Terrain(x, y, go_size){}
    void self_render(std::string& wmap, int height, int width)
    {
        for (int i = 0; i < go_size; ++i)
        {
            for (int j = 0; j < go_size; ++j)
            {
                assert(wmap[(y - 1 + i) * (width * 2 + 1) + (x - 1 + j) * 2] == '.');
                wmap[(y - 1 + i) * (width * 2 + 1) + (x - 1 + j) * 2] = this -> symbol();
            }
        }
    }
};

class World
{
protected:
    friend void launch();
    World(){}
    World(int width, int height):width(width), height(height)
    {
        wmap.resize(height * (2 * width + 1) + 1);
        wmap[height * (2 * width + 1)] = '\0';
        this->render();
    }
    ~World()
    {
        gameobjects.clear();
        std::vector<GameObject*>().swap(gameobjects);
    }
    void show() const
    {
        std::cout << wmap;
    }
    void add_object(GameObject* new_object)
    {
        gameobjects.push_back(new_object);
        this->render();
    }
    GameObject* operator[](const int& index)
    {
        return gameobjects[index];
    }
    void go_move(int index, int x, int y)
    {
        gameobjects[index - 1] -> go_move(x, y);
        this->render();
    }
    void dump()
    {
        std::cout << "Dumping " << gameobjects.size() << " objects:\n";
        for(auto it = gameobjects.begin(); it != gameobjects.end(); ++it)
        {
            std::cout << it - gameobjects.begin() + 1 << ". ";
            (*it) -> print();
        }
    }
private:
    void render()
    {
        for (int i = 0; i < height; ++i)
        {
            for (int j = 0; j < width; ++j)
            {
                wmap[i * (width * 2 + 1) + j * 2] = '.';
                wmap[i * (width * 2 + 1) + j * 2 + 1] = ' ';
            }
            wmap[i * (width * 2 + 1) + width * 2] = '\n';
        }
        wmap[height * (2 * width + 1)] = '\0';
        if (gameobjects.size() != 0)
        {
            for (auto it = gameobjects.begin(); it != gameobjects.end(); ++it)
            {
                (*it) -> self_render(wmap, height, width);
            }
        }
    }
    int width, height;
    std::vector<GameObject*> gameobjects;
    std::string wmap;
    World(const World&);
    void operator= (const World&);
};

void launch()
{
    World* wor;
    GameObject* obj;
    std::string mystring;
    std::cout << "> ";
    mystring = "help";
    int x;
    int y;
    int hp;
    int go_size;
    int depth;
    int height;
    int id;
    while(mystring != "quit")
    {
        if(mystring == "map")
        {
            int width;
            int height;
            std::cin >> width >> height;
            wor = new World(width, height);
        }
        if(mystring == "show")
        {
            (*wor).show();
        }
        if(mystring == "create")
        {
            std::cin >> mystring;
            if(mystring == "hero")
            {
                std::cin >> x >> y >> hp >> mystring;
                obj = new Hero(x, y, hp, mystring);
                (*wor).add_object(obj);
            }
            if(mystring == "monster")
            {
                std::cin >> x >> y >> hp;
                obj = new Monster(x, y, hp);
                (*wor).add_object(obj);
            }
            if(mystring == "forest")
            {
                std::cin >> x >> y >> go_size;
                obj = new Forest(x, y, go_size);
                (*wor).add_object(obj);
            }
            if(mystring == "lake")
            {
                std::cin >> x >> y >> go_size >> depth;
                obj = new Lake(x, y, go_size, depth);
                (*wor).add_object(obj);
            }
            if(mystring == "mountain")
            {
                std::cin >> x >> y >> go_size >> height;
                obj = new Mountain(x, y, go_size, height);
                (*wor).add_object(obj);
            }
        }
        if(mystring == "move")
        {
            std::cin >> id >> x >> y;
            (*wor).go_move(id, x, y);
        }
        if(mystring == "dump")
        {
            (*wor).dump();
        }
        if(mystring == "help")
        {
            std::cout << "valid commands are:\n";
            std::cout << "/values to enter are marked with $\n";
            std::cout << "map $height $width                  to create a map\n";
            std::cout << "show                                to display the map\n";
            std::cout << "move $index $x $y                   to move object with index to this point\n";
            std::cout << "create hero $x $y $hp $name         to create hero with these parameters\n";
            std::cout << "create monster $x $y $hp            to create monster with these parameters\n";
            std::cout << "create lake $x $y $size $depth      to create lake with these parameters\n";
            std::cout << "create mountain $x $y $size $height to create mountain with these parameters\n";
            std::cout << "create forest $x $y $size           to create forest with these parameters\n";
            std::cout << "dump                                to show the description of existing objects\n";
            std::cout << "help                                to show valid commands\n";
            std::cout << "quit                                to exit map editor\n";
        }
        std::cout << "\n" << "> ";
        std::cin >> mystring;
    }
    delete wor;
}

int main()
{
    launch();
    return 0;
}
