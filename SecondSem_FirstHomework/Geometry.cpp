#include <cmath>
#include <iostream>
#include <vector>
#include <assert.h>
#include <functional>
#include <algorithm>
const double pi = 3.141592653589793238463;


class Point
{
public:
    bool operator<=(Point a)
    {
        return (x < a.x && y< a.y) || (x == a.x && y == a.y);
    }
    std::ostream& operator<<(std::ostream& stream)
    {
        stream << "(" << x << ", " << y << ")\n";
        return stream;
    }
    Point& operator= (Point a)
    {
        x = a.x;
        y = a.y;
    }
    Point(float a, float b): x(a), y(b){}
    Point(){}
    float x;
    float y;
};

class Segment
{
public:
    Segment(Point a, Point b): s_begin(a), s_end(b) {}
    float cross_product (Point& a)
    {
        return ((this -> s_end.x - this -> s_begin.x) * (a.y - this->s_begin.y) - (this->s_end.y - this->s_begin.y) * (a.x - this->s_begin.x)) / 2;
    }
    bool point_belongs_to_line(Point& a)
    {
        if(this -> cross_product(a) == 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    bool are_crossing (Segment& seg)
    {
        if(this->cross_product(seg.s_end) * this->cross_product(seg.s_begin) <= 0 && (seg.cross_product(this->s_begin) * seg.cross_product(this->s_end)) <= 0)
        {
            if(!seg.point_belongs_to_line(this ->s_begin) && !seg.point_belongs_to_line(this -> s_end))
            {
                return 1;
            }
            if(this->point_belongs_to_segment(seg.s_begin) || this->point_belongs_to_segment(seg.s_end) || seg.point_belongs_to_segment(this->s_begin) || seg.point_belongs_to_segment(this->s_end))
            {
                return 1;
            }
            return 0;
        }
        else
        {
            return 0;
        }
    }
    bool point_belongs_to_segment(Point& a)
    {
        if(this -> cross_product(a) == 0 && (a.x <= std::max(this->s_begin.x, this->s_end.x) && a.x >= std::min(this->s_begin.x, this->s_end.x))
           &&(a.y <= std::max(this->s_begin.y, this->s_end.y) && a.y >= std::min(this->s_begin.y, this->s_end.y)))
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    Point find_crossing(Segment seg)
    {
        assert(this -> are_crossing(seg));
        Point mid((this->s_begin.x + this->s_end.x) / 2, (this->s_begin.y + this->s_end.y) / 2);
        Point seg_beg = this->s_begin;
        Point seg_end = this->s_end;
        while((seg.point_belongs_to_segment(mid)) != 1)
        {
            Segment fpart(seg_beg, mid);
            Segment spart(mid, seg_end);
            if(seg.are_crossing(fpart))
            {
                seg_end = mid;
            }
            else
            {
                seg_beg = mid;
            }
            mid.x = (seg_beg.x + seg_end.x) / 2;
            mid.y = (seg_beg.y + seg_end.y) / 2;
        }
        return mid;
    }
    Point s_begin;
    Point s_end;
};

class Shape
{
public:
    virtual float find_area() {}
    virtual bool is_on_borderline(Point point) {}
    virtual bool is_inside_shape(Point point) {}
    virtual Point find_crossing_point(Segment seg) {}
};

class Circle : public Shape
{
public:
    Circle(Point a, float rad): center(a), radius(rad){}
    float find_area()
    {
        return pi * radius * radius;
    }
    bool is_on_borderline(Point point)
    {
        float x_len = abs(center.x - point.x);
        float y_len = abs(center.y - point.y);
        return x_len * x_len + y_len * y_len == radius * radius;
    }
    bool is_inside_shape(Point point)
    {
        float x_len = abs(center.x - point.x);
        float y_len = abs(center.y - point.y);
        return x_len * x_len + y_len * y_len < radius * radius;
    }
    Point find_crossing_point(Segment seg)
    {
        assert(this -> is_inside_shape(seg.s_begin) && !this -> is_inside_shape(seg.s_end)
               || this -> is_inside_shape(seg.s_end) && !this -> is_inside_shape(seg.s_begin));
        Point mid((seg.s_begin.x + seg.s_end.x) / 2, (seg.s_begin.y + seg.s_end.y) / 2);
        Point seg_beg = seg.s_begin;
        Point seg_end = seg.s_end;
        while((this->is_on_borderline(mid)) != 1)
        {
            Segment fpart(seg_beg, mid);
            Segment spart(mid, seg_end);
            if(this->is_inside_shape(mid))
            {
                seg_beg = mid;
            }
            else
            {
                seg_end = mid;
            }
            mid.x = (seg_beg.x + seg_end.x) / 2;
            mid.y = (seg_beg.y + seg_end.y) / 2;
        }
        return mid;
    }
private:
    Point center;
    float radius;
};

class Polygon : public Shape
{
public:
    float find_area() override
    {
        float Area = 0;
        auto max_x = max_element(segments.begin(), segments.end(), [](Segment a, Segment b){return a.s_begin.x < b.s_begin.x;});
        auto max_y = max_element(segments.begin(), segments.end(), [](Segment a, Segment b){return a.s_begin.y < b.s_begin.y;});
        Point pivot(max_x -> s_begin.x + 1.258, max_y -> s_begin.y + 0.753);
        for(auto it = segments.begin(); it != segments.end(); ++it)
        {
            Area += it -> cross_product(pivot);
        }
        if(Area < 0)
        {
            return Area * -1;
        }
        else
        {
            return Area;
        }
    }
    bool is_on_borderline(Point a)
    {
        for(auto it = segments.begin(); it != segments.end(); ++it)
        {
            if (it -> point_belongs_to_segment(a))
            {
                return 1;
            }
        }
        return 0;
    }
    bool is_inside_shape(Point p)
    {
        auto max_x = max_element(segments.begin(), segments.end(), [](Segment a, Segment b){return a.s_begin.x < b.s_begin.x;});
        auto max_y = max_element(segments.begin(), segments.end(), [](Segment a, Segment b){return a.s_begin.y < b.s_begin.y;});
        Point pivot(max_x -> s_begin.x + 1.258, max_y -> s_begin.y + 0.753);
        Segment seg(pivot, p);
        for(int i = 0; i < segments.size(); ++i)
        {
            if(seg.point_belongs_to_segment(p))
            {
                seg.s_begin.x += 1;
                i = -1;
            }
        }
        int counter = 0;
        for(auto it = segments.begin(); it != segments.end(); ++it)
        {
            if (it -> are_crossing(seg))
            {
                ++counter;
            }
        }
        std::cout << "!!!" << counter << "\n";
        return counter % 2;
    }
    bool IsPolygon()
    {
        for(auto it1 = segments.begin(); (it1 + 1) != segments.end(); ++it1)
        {
            for(auto it2 = (it1 + 2); it2 != segments.end(); ++it2)
            {
                if(it1 -> are_crossing(*it2) && (it1 != segments.begin() || (it2 + 1) != segments.end()))
                {
                    return 0;
                }
            }
        }
        return 1;
    }
    Polygon(std::vector<Point>& vect)
    {
        assert(vect.size() >= 3);
        Segment seg(vect[0], vect[0]);
        for(auto it = vect.begin() + 1; it != vect.end(); ++it)
        {
            seg.s_begin = seg.s_end;
            seg.s_end = *it;
            segments.push_back(seg);
        }
        seg.s_begin = seg.s_end;
        seg.s_end = *(vect.begin());
        segments.push_back(seg);
        assert(this -> IsPolygon());
    }
    Point find_crossing_point(Segment seg)
    {
        for (auto it = segments.begin(); it != segments.end(); ++it)
        {
            if(seg.are_crossing(*it))
            {
                return seg.find_crossing(*it);
            }
        }
        assert(0);
    }
protected:
    std::vector<Segment> segments;
};

class ConvexPolygon : public Polygon
{
public:
    bool is_convex()
    {
        bool sign = 0;
        if (segments[0].cross_product(segments[1].s_end) < 0)
        {
            sign = 1;
        }
        for(auto it = segments.begin() + 1; it + 1!= segments.end(); ++it)
        {
            if((it -> cross_product((it + 1) -> s_end) < 0) != sign)
            {
                return 0;
            }
        }
        return 1;
    }
    ConvexPolygon(std::vector<Point>& vect):Polygon(vect)
    {
        assert(this -> is_convex());
    }
private:
};

class Triangle: public ConvexPolygon
{
public:
    Triangle(std::vector<Point>& vect):ConvexPolygon(vect)
    {
        assert(segments.size() == 3);
    }
};

class Rectanlge: public ConvexPolygon
{
public:
    bool is_rectangular()
    {
        float x2;
        float x1;
        float y1;
        float y2;
        for(auto it = segments.begin(); (it + 1) != segments.end(); ++it)
        {
            x1 = it->s_begin.x - it->s_end.x;
            y1 = it->s_begin.y - it->s_end.y;
            x2 = (it + 1)->s_end.x - (it + 1)->s_begin.x;
            y2 = (it + 1)->s_end.y - (it + 1)->s_begin.y;
            return (x1 * y2 - y1 * x2) * (x1 * y2 - y1 * x2) ==(x1 * x1 + y1 * y1) * (x2 * x2 + y2 * y2);
        }
    }
    Rectanlge(std::vector<Point>& vect):ConvexPolygon(vect)
    {
        assert(segments.size() == 4);
        assert(this -> is_rectangular());
    }
};

class Square: public Rectanlge
{
public:
    bool is_square()
    {
        float x = segments[0].s_end.x - segments[0].s_begin.x;
        float y = segments[0].s_end.y - segments[0].s_begin.y;
        float len = x * x + y * y;
        for(auto it = segments.begin() + 1; it != segments.end(); ++it)
        {
            x = it->s_begin.x - it->s_end.x;
            y = it->s_begin.y - it->s_end.y;
            if(len != x * x + y * y)
            {
                return 0;
            }
        }
        return 1;
    }
    Square(std::vector<Point>& vect):Rectanlge(vect)
    {
        assert(segments.size() == 4);
    }
};

void launch()
{
    std::cout << "valid commands: \n";
    std::cout << "values to enter are marked with $ \n";
    std::cout << "add_apex $x $y                    creates an apex and pushes it to vector \n";
    std::cout << "create_convex_polygon             creates convex polygon from current vector\n";
    std::cout << "create_polygon                    creates polygon from current vector\n";
    std::cout << "create_triangle                   creates triangle from current vector\n";
    std::cout << "create_rectangle                  creates rectangle from current vector\n";
    std::cout << "create_square                     creates square from current vector\n";
    std::cout << "create_circle $rad $x $y          creates circle with entered values \n";
    std::cout << "find_area                         displays created shape's area\n";
    std::cout << "is_on_borderline $x $y            checks if point belongs to borderline\n";
    std::cout << "is_inside_shape $x $y             checks if point is inside shape\n";
    std::cout << "find_crossing_point $x $y $xf $yf checks if segment crosses shape\n";
    float x, y, x2, y2;
    float radius;
    std::string mystring;
    std::vector<Point> segments;
    Shape* shape = new Shape;
    std::cout << "> ";
    while(true)
    {
        std::cin >> mystring;
        if(mystring == "add_apex")
        {
            std::cin >> x >> y;
            Point a(x, y);
            segments.push_back(a);
        }
        if(mystring == "create _convex_polygon")
        {
            delete shape;
            shape = new ConvexPolygon(segments);
            segments.clear();
        }
        if(mystring == "create_polygon")
        {
            delete shape;
            shape = new Polygon(segments);
            segments.clear();
        }
        if(mystring == "create_triangle")
        {
            delete shape;
            shape = new Triangle(segments);
            segments.clear();
        }
        if(mystring == "create_rectangle")
        {
            delete shape;
            shape = new Rectanlge(segments);
            segments.clear();
        }
        if(mystring == "create_square")
        {
            delete shape;
            shape = new Square(segments);
            segments.clear();
        }
        if(mystring == "create_circle")
        {
            delete shape;
            std::cin >> radius;
            std::cin >> x >> y;
            Point center(x, y);
            shape = new Circle(center, radius);
        }
        if(mystring == "exit")
        {
            delete shape;
            break;
        }
        if(mystring == "find_area")
        {
            std::cout << shape -> find_area() << "\n";
        }
        if(mystring == "is_on_borderline")
        {
            std::cin >> x >> y;
            Point a(x, y);
            std::cout << shape -> is_on_borderline(a) << "\n";
        }
        if(mystring == "is_inside_shape")
        {
            std::cin >> x >> y;
            Point a(x, y);
            std::cout << shape -> is_inside_shape(a) << "\n";
        }
        if(mystring == "find_crossing_point")
        {
            std::cin >> x >> y;
            std::cin >> x2 >> y2;
            Point a(x, y);
            Point b(x2, y2);
            Segment seg(a, b);
            (shape -> find_crossing_point(seg)) << std::cout  << "\n";
        }
        std::cout << "> ";
    }
}

int main()
{
    launch();
    return 0;
}
