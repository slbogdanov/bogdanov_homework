#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <cstdlib>
#include <assert.h>

class Publication
{
public:
    virtual std::string get_name() {}
    virtual void set_name(std::string new_name) {}
    virtual std::string get_author() {}
    virtual void set_author(std::string new_author) {}
    virtual std::string get_publishing_house() {}
    virtual void set_publishing_house(std::string new_publishing_house) {}
    virtual int get_release_date() {}
    virtual void set_release_date(int new_release_date) {}
    virtual ~Publication();
    void give_copy()
    {
        --current_amount;
    }
    void remove_one_copy()
    {
        --current_amount;
        --general_amount;
    }
    bool there_is_a_copy ()
    {
        return current_amount > 0;
    }
    void add_copy()
    {
        ++current_amount;
        ++general_amount;
    }
    int get_current_amount()
    {
        return current_amount;
    }
    int get_general_amount()
    {
        return general_amount;
    }
    Publication()
    {
        general_amount = 1;
        current_amount = 1;
    }
protected:
    int general_amount;
    int current_amount;
};
Publication::~Publication(){}

class Book : public Publication
{
public:
    void set_release_date (int new_release_date)
    {
        release_date = new_release_date;
    }
    int get_release_date () const
    {
        return release_date;
    }
    void set_amount_of_pages (int new_amount_of_pages)
    {
        amount_of_pages = new_amount_of_pages;
    }
    int get_amount_of_pages() const
    {
        return amount_of_pages;
    }
    void set_name (std::string new_name)
    {
        name = new_name;
    }
    void set_piblishing_house (std::string new_publishing_house)
    {
        publishing_house = new_publishing_house;
    }
    std::string get_publishing_house ()
    {
        return publishing_house;
    }
    std::string get_publishing_house () const
    {
        return publishing_house;
    }
    std::string get_name ()
    {
        return name;
    }
    std::string get_name () const
    {
        return name;
    }
    Book(){}
    Book(const Book& anotherbook)
    {
        author = anotherbook.get_author();
        name = anotherbook.get_name();
        publishing_house = anotherbook.get_publishing_house();
        amount_of_pages = anotherbook.get_amount_of_pages();
        release_date = anotherbook.get_release_date();
        content = anotherbook.get_content();
    }
    void set_author (std::string new_author)
    {
        author = new_author;
    }
    std::string get_author ()
    {
        return author;
    }
    std::string get_author () const
    {
        return author;
    }
    void add_content (int page, std::string page_content)
    {

        content[page] = page_content;
    }
    std::map<int, std::string> get_content ()
    {
        return content;
    }
    std::map<int, std::string> get_content () const
    {
        return content;
    }
private:
    std::string author;
    std::string name;
    std::string publishing_house;
    int amount_of_pages;
    int release_date;
    std::map<int, std::string> content;
};

class Collection : public Publication
{
public:
    void set_amount_of_tomes (int new_amount_of_tomes)
    {
        amount_of_tomes = new_amount_of_tomes;
    }
    int get_amount_of_tomes() const
    {
        return amount_of_tomes;
    }
    int get_amount_of_tomes()
    {
        return amount_of_tomes;
    }
    void set_author (std::string new_author)
    {
        author = new_author;
    }
    std::string get_author () const
    {
        return author;
    }
    std::string get_author ()
    {
        return author;
    }
    void set_name (std::string new_name)
    {
        name = new_name;
    }
    std::string get_name () const
    {
        return name;
    }
    std::string get_name ()
    {
        return name;
    }
    void add_book(Book new_book)
    {
        books.push_back(new_book);
    }
    void remove_book (int index)
    {
        auto it = books.begin();
        ++index;
        while(it != books.end() && index > 0)
        {
            ++it;
            --index;
        }
        books.erase(it);
    }
    std::vector<Book> get_books() const
    {
        return books;
    }
    std::vector<Book> get_books()
    {
        return books;
    }
    Book get_book (int index) const
    {
        return books[index];
    }
    Book get_book (int index)
    {
        return books[index];
    }
    void set_piblishing_house (std::string new_publishing_house)
    {
        publishing_house = new_publishing_house;
    }
    std::string get_publishing_house () const
    {
        return publishing_house;
    }
    std::string get_publishing_house ()
    {
        return publishing_house;
    }
    void set_release_date (int new_release_date)
    {
        release_date = new_release_date;
    }
    int get_release_date () const
    {
        return release_date;
    }
    int get_release_date ()
    {
        return release_date;
    }
    Collection(){}
    Collection(const Collection& anothercollection)
    {
        author = anothercollection.get_author();
        name = anothercollection.get_name();
        publishing_house = anothercollection.get_publishing_house();
        amount_of_tomes = anothercollection.get_amount_of_tomes();
        release_date = anothercollection.get_release_date();
        books = anothercollection.get_books();
    }
private:
    std::string name;
    std::string author;
    int amount_of_tomes;
    std::vector<Book> books;
    std::string publishing_house;
    int release_date;
};

class Magazine : public Publication
{
public:
    void set_period (int new_period)
    {
        period = new_period;
    }
    int get_period() const
    {
        return period;
    }
    int get_period()
    {
        return period;
    }
    void set_author (std::string new_author)
    {
        author = new_author;
    }
    std::string get_author () const
    {
        return author;
    }
    std::string get_author ()
    {
        return author;
    }
    void set_name (std::string new_name)
    {
        name = new_name;
    }
    std::string get_name () const
    {
        return name;
    }
    std::string get_name ()
    {
        return name;
    }
    void add_book(Book new_book)
    {
        books.push_back(new_book);
    }
    void remove_book (int index)
    {
        auto it = books.begin();
        ++index;
        while(it != books.end() && index > 0)
        {
            ++it;
            --index;
        }
        books.erase(it);
    }
    Book get_book (int index) const
    {
        return books[index];
    }
    Book get_book (int index)
    {
        return books[index];
    }
    void set_piblishing_house (std::string new_publishing_house)
    {
        publishing_house = new_publishing_house;
    }
    std::string get_publishing_house () const
    {
        return publishing_house;
    }
    std::string get_publishing_house ()
    {
        return publishing_house;
    }
    void set_release_date (int new_release_date)
    {
        release_date = new_release_date;
    }
    int get_release_date () const
    {
        return release_date;
    }
    int get_release_date ()
    {
        return release_date;
    }
    std::vector<Book> get_books() const
    {
        return books;
    }
    std::vector<Book> get_books()
    {
        return books;
    }
    Magazine(){}
    Magazine(const Magazine& anothermagazine)
    {
        author = anothermagazine.get_author();
        name = anothermagazine.get_name();
        publishing_house = anothermagazine.get_publishing_house();
        period = anothermagazine.get_period();
        release_date = anothermagazine.get_release_date();
        books = anothermagazine.get_books();
    }
private:
    std::string name;
    std::string author;
    int period;
    int release_date;
    std::string publishing_house;
    std::vector<Book> books;
};

class Library
{
public:
    class LibIterator
    {
    public:
        LibIterator(std::vector<Publication*>::iterator sec_it): it(sec_it){}
        LibIterator(const Library::LibIterator& sec_it)
        {
            it = sec_it.get_it();
        }
        LibIterator& operator= (const LibIterator& sec_it)
        {
            it = sec_it.get_it();
            return *this;
        }
        bool operator!= (const LibIterator& sec_it)
        {
            return it != sec_it.get_it();
        }
        LibIterator& operator++ ()
        {
            ++it;
            return *this;
        }
        std::vector<Publication*>::iterator get_it() const
        {
            return it;
        }
        Publication* operator* ()
        {
            return *it;
        }
    private:
        std::vector<Publication*>::iterator it;
    };
    typedef  class LibIterator iterator;
    iterator begin()
    {
        iterator it(publications.begin());
        return it;
    }
    iterator end()
    {
        iterator it(publications.end());
        return it;
    }
    iterator search_by_name (std::string name)
    {
        for(auto it = this -> begin(); (it != this -> end()); ++it)
        {
            if(name.compare((**it).get_name()) == 0)
            {
                return it;
            }
        }
        return this -> end();
    }
    iterator search_by_publishing_house (std::string publishing_house)
    {
        for(iterator it = this -> begin(); it != this -> end(); ++it)
        {
            if(publishing_house.compare((**it).get_publishing_house()) == 0)
            {
                return it;
            }
        }
        return this -> end();
    }
    iterator search_by_author (std::string author)
    {
        for(iterator it = this -> begin(); it != this -> end(); ++it)
        {
            if(author.compare((**it).get_author()) == 0)
            {
                return it;
            }
        }
        return this -> end();
    }
    iterator search_by_release_date (int release_date)
    {
        for(iterator it = this -> begin(); it != this -> end(); ++it)
        {
            if((**it).get_release_date() == release_date)
            {
                return it;
            }
        }
        return this -> end();
    }
    void add_copy_by_name(std::string name)
    {
        auto it = this -> search_by_name(name);
        if(it != this -> end())
        {
            (**it).add_copy();
        }
        ++amount_of_publications;
    }
    void give_copy(std::string name)
    {
        auto it = this -> search_by_name(name);
        if(it != this -> end())
        {
            assert((**it).there_is_a_copy());
            (**it).give_copy();
        }
        --amount_of_publications;
    }
    void remove_one_copy(std::string name)
    {
        auto it = this -> search_by_name(name);
        if(it != this -> end())
        {
            assert((**it).there_is_a_copy());
            (**it).remove_one_copy();
        }
        --amount_of_publications;
    }
    void add_publication (Publication* pub)
    {
        auto it = this -> search_by_name(pub -> get_name());
        if(it != this -> end())
        {
            (**it).add_copy();
        }
        else
        {
            Publication* pub2;
            if (dynamic_cast<const Book*>(pub))
            {
                pub2 = new Book(*dynamic_cast<const Book*>(pub));
            }
            if (dynamic_cast<const Magazine*>(pub))
            {
                pub2 = new Magazine(*dynamic_cast<const Magazine*>(pub));
            }
            if (dynamic_cast<const Collection*>(pub))
            {
                pub2 = new Collection(*dynamic_cast<const Collection*>(pub));
            }
            publications.push_back(pub2);
            ++amount_of_publications;
        }
    }
    void remove_publication (Publication* pub)
    {
        auto it = this -> search_by_name(pub -> get_name());
        publications.erase(it.get_it());
    }
    Library()
    {
        amount_of_publications = 0;
    }
    int get_amount_of_publications()
    {
        return amount_of_publications;
    }
    bool there_is_a_copy(Publication* pub)
    {
        auto it = this -> search_by_name(pub -> get_name());
        return (**it).there_is_a_copy();
    }
    int get_current_amount(Publication* pub)
    {
        auto it = this -> search_by_name(pub -> get_name());
        return (**it).get_current_amount();
    }
    int get_general_amount(Publication* pub)
    {
        auto it = this -> search_by_name(pub -> get_name());
        return (**it).get_general_amount();
    }
    ~Library()
    {
        for(auto it = this -> begin(); it != this -> end(); ++it)
        {
            delete (*it);
        }
    }
private:
    std::vector<Publication*> publications;
    int amount_of_publications;
};


int main()
{
    Book book;
    book.add_content(100, "fairytale");
    book.add_content(120, "novel");
    std::map<int, std::string> content;
    content = book.get_content();
    std::cout << "added book content:\n";
    for(auto it = content.begin(); it != content.end(); ++it)
    {
        std::cout << it -> first << " " << it -> second << "\n";
    }
    Library lib;
    Publication* pub1 = new Book;
    pub1 -> set_name("some book");
    lib.add_publication(pub1);
    Publication* pub2 = new Book;
    pub2 -> set_name("another book");
    lib.add_publication(pub2);
    std::cout << "added some book and another book to library.\n";
    std::cout << "amount of copies of some book:\n";
    std::cout << lib.get_current_amount(pub1) << "\n";
    lib.add_copy_by_name(pub1 -> get_name());
    std::cout << "added another copy of some book.\n";
    std::cout << "new amount of copies of some book:\n";
    std::cout << lib.get_current_amount(pub1) << "\n";
    std::cout << "overall amount of publications:\n";
    std::cout << lib.get_amount_of_publications() << "\n";
    lib.remove_one_copy(pub1 -> get_name());
    std::cout << "removed one copy of some book.\n";
    std::cout << "overall amount of publications:\n";
    std::cout << lib.get_amount_of_publications() << "\n";
    std::cout << "amount of copies of some book:\n";
    std::cout << lib.get_current_amount(pub1) << "\n";
    lib.give_copy(pub1 -> get_name());
    std::cout << "gave one copy of some book.\n";
    std::cout << "current amount of copies of some book:\n";
    std::cout << lib.get_current_amount(pub1) << "\n";
    std::cout << "general amount of copies of some book:\n";
    std::cout << lib.get_general_amount(pub1) << "\n";
    delete pub1;
    delete pub2;
    return 0;
}
