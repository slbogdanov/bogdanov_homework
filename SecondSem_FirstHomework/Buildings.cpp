#include <iostream>
#include <string.h>
#include <vector>

class Human
{
public:
    virtual std::string GetName() const {return 0;}
    Human (const std::string& a, const std::string& b): firstname(a), lastname(b) {}
    virtual ~Human() = 0;
protected:
    std::string firstname;
    std::string lastname;
};
Human::~Human() {}

class Student : public Human
{
public:
    std::string GetName() const
    {
        return firstname + " " + lastname;
    }
    Student(const std::string& a, const std::string&b): Human(a, b){};
    ~Student()
    {
        firstname.clear();
        lastname.clear();
    }
private:
    int course;
};

class Worker : public Human
{
public:
    std::string GetName() const
    {
        return firstname + " " + lastname;
    }
    Worker(const std::string& a, const std::string&b): Human(a, b){};
    ~Worker()
    {
        firstname.clear();
        lastname.clear();
    }
private:
    std::string job;
};

class Building
{
public:
    virtual bool Permission(const Human* h){return 0;}
    virtual void GrantPermission(Human* h) {}
    Building(const std::string& a, const std::string& b): location(a), name(b) {}
    virtual ~Building() = 0;
protected:
    std::string location;
    std::string name;
};
Building::~Building() {}

class Laboratory : public Building
{
protected:
    std::vector <std::string> ListOfPermissions;
public:
    Laboratory(const std::string& a, const std::string& b): Building(a, b){};
    bool Permission(const Human* h)
    {
        if (dynamic_cast<const Worker*>(h))
        {
            return true;
        }
        if (dynamic_cast<const Student*>(h))
        {
            for(std::vector <std::string>::iterator it = ListOfPermissions.begin(); it!= ListOfPermissions.end(); ++it)
            {
                if (h -> GetName() == *it)
                {
                    return true;
                }
            }
        }
        return false;
    }
    ~Laboratory()
    {
        location.clear();
        name.clear();
        ListOfPermissions.clear();
    }
    void GrantPermission(Human* h)
    {
        ListOfPermissions.push_back(h -> GetName());
    }
};

class OrdinaryBuilding : public Building
{
public:
    void GrantPermission(Human* h){}
    bool Permission(const Human* h)
    {
        if(dynamic_cast<const Worker*>(h) || dynamic_cast<const Student*>(h))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    ~OrdinaryBuilding()
    {
        location.clear();
        name.clear();
    }
    OrdinaryBuilding(const std::string& a, const std::string& b): Building(a, b){};
};

int main()
{
    std::string mystr1, mystr2;
    std::cin >> mystr1 >> mystr2;
    Human* student_a = new Student(mystr1, mystr2);
    std::cin >> mystr1 >> mystr2;
    Human* worker_b = new Worker(mystr1, mystr2);
    std::cin >> mystr1 >> mystr2;
    Building* lab_a = new Laboratory(mystr1, mystr2);
    std::cout << lab_a -> Permission(worker_b) << " \n" ;
    std::cout << lab_a -> Permission(student_a) << " \n" ;
    lab_a -> GrantPermission(student_a);
    std::cout << lab_a -> Permission(student_a) << " \n" ;
    delete student_a;
    delete worker_b;
    delete lab_a;
    return 0;
}
