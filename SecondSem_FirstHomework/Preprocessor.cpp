#include <iostream>
#include <string.h>
#include <vector>
#include <fstream>

int main()
{
    std::ifstream input;
    std::vector <std::string> lookfor;
    std::vector <std::string> replacewith;
    std::string mystring;
    std::ofstream output;
    input.open("input.txt");
    output.open("subfile.txt");
    char symb;
    char nextsymb;
    while(input.get(symb))
    {
        if(symb == '/' && input.get(nextsymb))
        {
            if(nextsymb == '/')
            {
                getline(input, mystring);
                output << std::endl;
            }
            if(nextsymb == '*')
            {
                input.get(symb);
                input.get(nextsymb);
                symb = nextsymb;
                while(input.get(nextsymb))
                {
                    if (symb == '*' && nextsymb == '/')
                    {
                        break;
                    }
                    symb = nextsymb;
                }
            }
        }
        else
        {
            output << symb;
        }
    }
    int i;
    input.close();
    output.close();
    input.open("subfile.txt");
    output.open("output.txt");
    int isreplaced = 0;
    while(input >> mystring)
    {
        for(i = 0; i < lookfor.size(); ++i)
        {
            if(mystring == lookfor[i])
            {
                mystring = replacewith[i];
                isreplaced = 1;
            }
        }
        if(isreplaced)
        {
            output << mystring;
        }
        if(mystring == "#define")
        {
            input >> mystring;
            lookfor.push_back(mystring);
            input >> mystring;
            replacewith.push_back(mystring);
            isreplaced = 1;
        }
        if(!isreplaced)
        {
            output << mystring;
        }
        isreplaced = 0;
        input.get(symb);
        output << symb;
    }
    input.close();
    output.close();
    return 0;
}
