#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <queue>

class Node
{
public:
    int color;
    std::vector<int> nextnodenum;
    int prevnode;
    Node()
    {
        color = 0;
        prevnode = -1;
    }
};

int strtoint(std::string str)
{
    int res = 0;
    int power = 1;
    for(int i = str.size(); i > 0; --i)
    {
        res += (str[i - 1] - '0') * power;
        power *= 10;
    }
    return res;
}

void input(std::vector<Node>& nodes, int& nbegin, int& nend, int& amount_of_nodes)
{
    std::cin >> amount_of_nodes;
    std::cin >> nbegin >> nend;
    std::string tmpstr;
    std::string anothertmpstr;
    int nodenum;
    nodes.resize(amount_of_nodes);
    getline(std::cin, tmpstr);
    for(int i = 0; i < amount_of_nodes; ++i)
    {
        getline(std::cin, tmpstr);
        for(int j = 0; j < tmpstr.size(); ++j)
        {
            while(tmpstr[j] != ' ' && j < tmpstr.size())
            {
                anothertmpstr += tmpstr[j];
                ++j;
            }
            nodenum = strtoint(anothertmpstr);
            anothertmpstr.clear();
            nodes[i].nextnodenum.push_back(nodenum - 1);
        }
        tmpstr.clear();
    }
}

int BFS (std::vector<Node>& nodes, std::vector<int>& route, int nbegin, int nend)
{
    int nodenum;
    std::queue<int> que;
    que.push(nbegin - 1);
    while(que.size() > 0)
    {
        nodenum = que.front();
        que.pop();
        if(nodes[nodenum].color == 0)
        {
            nodes[nodenum].color = 1;
            for(int i = 0; i < nodes[nodenum].nextnodenum.size(); ++i)
            {
                if(nodes[nodes[nodenum].nextnodenum[i]].color == 0)
                {
                    nodes[nodes[nodenum].nextnodenum[i]].prevnode = nodenum;
                    que.push(nodes[nodenum].nextnodenum[i]);
                }
            }
        }
    }
    nodenum = nend - 1;
    if(nodes[nodenum].prevnode == -1)
    {
        std::cout << "no suitable route found";
        return 0;
    }
    else
    {
        route.push_back(nodenum);
    }
    while(route[route.size() - 1] != nbegin - 1)
    {
        route.push_back(nodes[nodenum].prevnode);
        nodenum = route[route.size() - 1];
    }
    return 1;
}

void print_result(std::vector<int>& route)
{
    for(int i = route.size() - 1; i > 0; --i)
    {
        std::cout << route[i] + 1 << " " << route[i - 1] + 1 << "\n";
    }
}

int main()
{
    std::vector<Node> nodes;
    int amount_of_nodes;
    int nbegin, nend;
    input(nodes, nbegin, nend, amount_of_nodes);
    if(nend == nbegin)
    {
        std::cout << "no adapters needed";
        return 0;
    }
    std::vector<int> route;
    BFS(nodes, route, nbegin, nend);
    print_result(route);
    return 0;
}
