#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
#include <queue>

class Node
{
public:
    int color;
    std::vector<signed int> nextnodenum;
    Node()
    {
        color = 0;
    }
};

int strtoint(std::string str)
{
    int res = 0;
    int power = 1;
    for(int i = str.size(); i > 0; --i)
    {
        res += (str[i - 1] - '0') * power;
        power *= 10;
    }
    return res;
}

void DFS(std::vector<Node>& nodes, std::vector<int>& order, int num)
{
    if(nodes[num].color == 1 || (nodes[num].nextnodenum.size() > 0 && nodes[num].nextnodenum[0] == -1))
    {
        order.clear();
        order.push_back(-1);
    }
    if(nodes[num].color == 0)
    {
        nodes[num].color = 1;
        for(int i = 0; i < nodes[num].nextnodenum.size(); ++i)
        {
            DFS(nodes, order, nodes[num].nextnodenum[i]);
        }
        order.push_back(num);
        nodes[num].color = 2;
    }
}

int input(std::vector<Node>& nodes, int& amount_of_nodes)
{
    std::cin >> amount_of_nodes;
    nodes.resize(amount_of_nodes);
    std::string tmpstr;
    std::string anothertmpstr;
    signed int nodenum;
    getline(std::cin, tmpstr);
    for(int i = 0; i < amount_of_nodes; ++i)
    {
        getline(std::cin, tmpstr);
        for(int j = 0; j < tmpstr.size(); ++j)
        {
            while(tmpstr[j] != ' ' && j < tmpstr.size())
            {
                anothertmpstr += tmpstr[j];
                ++j;
            }
            nodenum = strtoint(anothertmpstr);
            anothertmpstr.clear();
            nodes[i].nextnodenum.push_back(nodenum - 1);
            if(nodenum == 0)
            {
                std::cout << "0";
                return 0;
            }
        }
        tmpstr.clear();
    }
    Node supernode;
    supernode.color = 0;
    for(int i = 0; i < amount_of_nodes; ++i)
    {
        supernode.nextnodenum.push_back(i);
    }
    nodes.push_back(supernode);
    return 1;
}

void print_result(std::vector<int>& order, int amount_of_nodes)
{
    if(order[0] == -1 || order.size() < amount_of_nodes + 1)
    {
        std::cout << "0";
        exit(0);
    }
    for(int i = 0; i  < order.size() - 1; ++i)
    {
        std::cout << order[i] + 1 << " ";
    }
}

int main()
{
    std::vector<Node> nodes;
    int amount_of_nodes;
    if (input(nodes, amount_of_nodes) == 0)
    {
        return 0;
    }
    std::vector<int> order;
    DFS(nodes, order, amount_of_nodes);
    print_result(order, amount_of_nodes);
    return 0;
}
