#include <vector>
#include <stdlib.h>
#include <iostream>
#include <assert.h>

template <typename T>
class Matrix
{
public:
    void inverse()
    {
        assert(this -> matrix_lenght() == this -> matrix_height());
        Matrix res_matr(this -> matrix_lenght(), this -> matrix_lenght());
        for(int i = 0; i < this -> matrix_lenght(); ++i)
        {
            res_matr[i][i] = 1;
        }
        int j;
        T coef;
        for(int i = 0; i < this -> matrix_lenght(); ++i)
        {
            for(j = i; j < this -> matrix_lenght(); ++j)
            {
                if(matr[j][i] != 0)
                {
                    break;
                }
            }
            if(j == this -> matrix_lenght())
            {
                break;
            }
            if(j != i)
            {
                matr[0].swap(matr[j]);
                res_matr[0].swap(res_matr[j]);
            }
            coef = 1 / matr[i][i];
            for(j = 0; j < this -> matrix_lenght(); ++j)
            {
                matr[i][j] *= coef;
                res_matr[i][j] *= coef;
            }
            for(j = 0; j < this -> matrix_lenght(); ++j)
            {
                if(matr[j][i] != 0 && j != i)
                {
                    coef = matr[j][i];
                    for(int k = 0; k < this -> matrix_lenght(); ++k)
                    {
                        matr[j][k] -= (matr[i][k] * coef);
                        res_matr[j][k] -= (res_matr[i][k] * coef);
                    }
                }
            }
        }
        *this = res_matr;
    }
    T get_determinant()
    {
        Matrix temp_matr = *this;
        int sign;
        temp_matr.get_upper_triangular_matrix(sign);
        T result = 1;
        for(int i = 0; i < temp_matr.matrix_lenght(); ++i)
        {
            result *= temp_matr[i][i];
        }
        return result * sign;
    }
    void get_upper_triangular_matrix (int& sign)
    {
        assert(this -> matrix_lenght() == this -> matrix_height());
        int j;
        T coef;
        sign = 1;
        for(int i = 0; i < this -> matrix_lenght(); ++i)
        {
            for(j = i; j < this -> matrix_lenght(); ++j)
            {
                if(matr[j][i] != 0)
                {
                    break;
                }
            }
            if(j == this -> matrix_lenght())
            {
                break;
            }
            if(j != i)
            {
                matr[0].swap(matr[j]);
                sign *= -1;
            }
            for(j = i + 1; j < this -> matrix_lenght(); ++j)
            {
                if(matr[j][i] != 0)
                {
                    coef = matr[j][i] / matr[i][i];
                    for(int k = i; k < this -> matrix_lenght(); ++k)
                    {
                        matr[j][k] -= (matr[i][k] * coef);
                    }
                }
            }
        }
    }
    Matrix power_of_matrix(int n)
    {
        assert(this -> matrix_lenght() == this -> matrix_height());
        Matrix res_matr(this -> matrix_lenght(), this -> matrix_lenght());
        for(int i = 0; i < this -> matrix_lenght(); ++i)
        {
            res_matr[i][i] = 1;
        }
        Matrix temp_matr = *this;
        int b = 0;
        while((1 << b) <= n)
        {
            if(((1 << b) & n) > 0)
            {
                res_matr = res_matr * temp_matr;
            }
            ++b;
            temp_matr = temp_matr * temp_matr;
        }
        return res_matr;
    }
    template <typename U>
    friend std::ostream& operator<< (std::ostream& out, const Matrix <U>& matrix);
    T trace()
    {
        assert(this -> matrix_lenght() == this -> matrix_height());
        T trace = 0;
        for(int i = 0; i < this -> matrix_lenght(); ++i)
        {
            trace += matr[i][i];
        }
        return trace;
    }
    void transposition()
    {
        Matrix res_matr(this -> matrix_lenght(), this -> matrix_height());
        for(int i = 0; i < this -> matrix_height(); ++i)
        {
            for(int j = 0; j < this -> matrix_lenght(); ++j)
            {
                res_matr[j][i] = matr[i][j];
            }
        }
        *this = res_matr;
    }
    Matrix operator* (Matrix& another_matr)
    {
        assert(this -> matrix_lenght() == another_matr.matrix_height());
        Matrix res_matr(this -> matrix_height(), another_matr.matrix_lenght());
        for(int i = 0; i < res_matr.matrix_height(); ++i)
        {
            for(int j = 0; j < res_matr.matrix_lenght(); ++j)
            {
                for(int k = 0; k < this -> matrix_lenght(); ++k)
                {
                    res_matr[i][j] += matr[i][k] * another_matr[k][j];
                }
            }
        }
        return res_matr;
    }
    Matrix& operator*= (T mult)
    {
        for(int i = 0; i < matr.size(); ++i)
        {
            for(int j = 0; j < matr[0].size(); ++j)
            {
                matr[i][j] * mult;
            }
        }
        return *this;
    }
    Matrix operator* (T mult)
    {
        Matrix res_matr(this -> matrix_height(), this -> matrix_lenght());
        for(int i = 0; i < matr.size(); ++i)
        {
            for(int j = 0; j < matr[0].size(); ++j)
            {
                res_matr[i][j] = matr[i][j] * mult;
            }
        }
        return res_matr;
    }
    Matrix& operator/= (T div)
    {
        for(int i = 0; i < matr.size(); ++i)
        {
            for(int j = 0; j < matr[0].size(); ++j)
            {
                matr[i][j] / div;
            }
        }
        return *this;
    }
    Matrix operator/ (T div)
    {
        Matrix res_matr(this -> matrix_height(), this -> matrix_lenght());
        for(int i = 0; i < matr.size(); ++i)
        {
            for(int j = 0; j < matr[0].size(); ++j)
            {
                res_matr[i][j] = matr[i][j] / div;
            }
        }
        return res_matr;
    }
    Matrix& operator-= (Matrix& another_matr)
    {
        assert(this -> matrix_height() == another_matr.matrix_height());
        assert(this -> matrix_lenght() == another_matr.matrix_lenght());
        for(int i = 0; i < matr.size(); ++i)
        {
            for(int j = 0; j < matr[0].size(); ++j)
            {
                matr[i][j] -= another_matr[i][j];
            }
        }
        return *this;
    }
    Matrix operator- (Matrix& another_matr)
    {
        assert(this -> matrix_height() == another_matr.matrix_height());
        assert(this -> matrix_lenght() == another_matr.matrix_lenght());
        Matrix res_matr(this -> matrix_height(), this -> matrix_lenght());
        for(int i = 0; i < matr.size(); ++i)
        {
            for(int j = 0; j < matr[0].size(); ++j)
            {
                res_matr[i][j] = matr[i][j] - another_matr[i][j];
            }
        }
        return res_matr;
    }
    Matrix& operator+= (Matrix& another_matr)
    {
        assert(this -> matrix_height() == another_matr.matrix_height());
        assert(this -> matrix_lenght() == another_matr.matrix_lenght());
        for(int i = 0; i < matr.size(); ++i)
        {
            for(int j = 0; j < matr[0].size(); ++j)
            {
                matr[i][j] += another_matr[i][j];
            }
        }
        return *this;
    }
    Matrix operator+ (Matrix& another_matr)
    {
        assert(this -> matrix_height() == another_matr.matrix_height());
        assert(this -> matrix_lenght() == another_matr.matrix_lenght());
        Matrix res_matr(this -> matrix_height(), this -> matrix_lenght());
        for(int i = 0; i < matr.size(); ++i)
        {
            for(int j = 0; j < matr[0].size(); ++j)
            {
                res_matr[i][j] = matr[i][j] + another_matr[i][j];
            }
        }
        return res_matr;
    }
    Matrix& operator= (Matrix another_matr)
    {
        this -> matrix_height_set(another_matr.matrix_height());
        this -> matrix_lenght_set(another_matr.matrix_lenght());
        for(int i = 0; i < matr.size(); ++i)
        {
            for(int j = 0; j < matr[0].size(); ++j)
            {
                matr[i][j] = another_matr[i][j];
            }
        }
        return *this;
    }
    Matrix()
    {
        matr.resize(1);
        matr[0].resize(1);
    }
    int matrix_height()
    {
        return matr.size();
    }
    int matrix_lenght()
    {
        return matr[0].size();
    }
    int matrix_height() const
    {
        return matr.size();
    }
    int matrix_lenght() const
    {
        return matr[0].size();
    }
    Matrix(int n, int m)
    {
        matr.resize(n);
        for(auto it = matr.begin(); it != matr.end(); ++it)
        {
            it -> resize(m);
        }
        for(int i = 0; i < n; ++i)
        {
            for(int j = 0; j < m; ++j)
            {
                matr[i][j] = 0;
            }
        }
    }
    const std::vector<T>& operator[](int ind) const
    {
        return matr[ind];
    }
    std::vector<T>& operator[](int ind)
    {
        return matr[ind];
    }
private:
    void matrix_height_set(int n)
    {
        matr.resize(n);
    }
    void matrix_lenght_set(int m)
    {
        for(auto it = matr.begin(); it != matr.end(); ++it)
        {
            it -> resize(m);
        }
    }
    std::vector<std::vector<T>> matr;
};

template <typename T>
std::ostream& operator<< (std::ostream& out, Matrix<T> const& matrix)
{
    for(int i = 0; i < matrix.matrix_height(); ++i)
    {
        for(int j = 0; j < matrix.matrix_lenght(); ++j)
        {
            out << matrix[i][j] << " ";
        }
        out << "\n";
    }
    return out;
}

void input (Matrix<float>& matrix)
{
    std::cout << "enter matrix itself>\n";
    for(int i = 0; i < matrix.matrix_height(); ++i)
    {
        for(int j = 0; j < matrix.matrix_lenght(); ++j)
        {
            std::cin >> matrix[i][j];
        }
    }
}


int main()
{
    std::cout << "enter matrix length and height>\n";
    int height, length;
    std::cin >> height >> length;
    Matrix<float> mat(height, length);
    input(mat);
    Matrix<float> another_matrix;
    another_matrix = mat;
    std::cout << "copied matrix >\n" << another_matrix;
    std::cout << "determinant is >\n" << another_matrix.get_determinant() << "\n";
    int power;
    std::cout << "enter desired power>\n";
    std::cin >> power;
    std::cout << "matrix in power>\n" << another_matrix.power_of_matrix(power);
    another_matrix.inverse();
    std::cout << "inversed matrix>\n" << another_matrix;
    return 0;
}
