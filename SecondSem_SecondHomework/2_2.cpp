#include <iostream>
#include <string.h>
#include <map>
#include <set>
#include <vector>

class Student
{
protected:
    std::map <std::string, int> exams;
    std::string name;
public:
    friend class Group;
    void AddExam(std::string exam, int& mark)
    {
        exams[exam] = mark;
    }
    void AddName(std::string newname)
    {
        name = newname;
    }
    std::string GetName() const
    {
        return name;
    }
    int AmountOfPassedExams() const
    {
        int examspassed = 0;
        for(std:: map <std::string, int> :: const_iterator it = exams.begin(); it != exams.end(); ++it)
        {
            if (it -> second > 2)
            {
                ++examspassed;
            }
        }
        return examspassed;
    }
    bool operator< (const class Student& anotherstudent) const
    {
        return name < anotherstudent.GetName();
    }
    void Erase()
    {
        exams.clear();
    }
};


class Group
{
    std::set <Student> group;
public:
    void GlobalDeduction(int k)
    {
        std::set <Student> :: const_iterator prev_it;
        for (std::set <Student> :: const_iterator it = group.begin(); it != group.end();)
        {
            if(it -> AmountOfPassedExams() < k)
            {
                prev_it = it++;
                group.erase(prev_it);
            }
            else
            {
                ++it;
            }
        }
    }
    void AddStudent(const class Student& student)
    {
        group.insert(student);
    }
    void ViewStudents()
    {
        for (std::set <Student> :: const_iterator it = group.begin(); it != group.end(); ++it)
        {
            std::cout << it -> GetName() << "\n";
        }
    }
    void RetakingStudents (std::string exam, std::vector<std::string>& retaking_students)
    {
        for(auto it = group.begin(); it != group.end(); ++it)
        {
            auto st = it -> exams.find(exam);
            if (st != it -> exams.end() && st -> second < 3)
            {
                retaking_students.push_back(it -> name);
            }
        }
    }
};



int main()
{
    class Student stdnt;
    class Group DIHT;
    std::string tmpstring;
    std::string anothertmpstring;
    std::vector<std::string> retaking_students;
    std::cout << "valid commands are:\n";
    std::cout << "values to enter are marked with $\n";
    std::cout << "add_name  $name         to change name of current student\n";
    std::cout << "add_exam $name $mark    to add exam to current student\n";
    std::cout << "add_student             to add current student to group and erase student\n";
    std::cout << "deduction $min          to deduct students with less than min passed exams\n";
    std::cout << "show_students           to display names of students in group\n";
    std::cout << "retaking_students $name to show students with low marks on entered exam\n";
    std::cout << "exit                    to exit\n";
    int tmpint;
    int minimum = 0;
    while(true)
    {
        std::cin >> tmpstring;
        if (tmpstring == "add_exam")
        {
            std::cin >> anothertmpstring >> tmpint;
            stdnt.AddExam(anothertmpstring, tmpint);
        }
        if (tmpstring == "exit")
        {
            break;
        }
        if (tmpstring == "add_student")
        {
            DIHT.AddStudent(stdnt);
            stdnt.Erase();
        }
        if (tmpstring == "add_name")
        {
            std::cin >> anothertmpstring;
            stdnt.AddName(anothertmpstring);
        }
        if (tmpstring == "deduction")
        {
            std::cin >> minimum;
            DIHT.GlobalDeduction(minimum);
            DIHT.ViewStudents();
        }
        if (tmpstring == "show_students")
        {
            DIHT.ViewStudents();
        }
        if (tmpstring == "retaking_students")
        {
            std::cin >> anothertmpstring;
            DIHT.RetakingStudents(anothertmpstring, retaking_students);
            for(auto it = retaking_students.begin(); it != retaking_students.end(); ++it)
            {
                std::cout << *it << "\n";
            }
            retaking_students.clear();
        }
    }
    return 0;
}
