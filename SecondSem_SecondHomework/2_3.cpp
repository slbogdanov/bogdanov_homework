#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>
#include <vector>
#include <assert.h>
#include <functional>
#include <algorithm>


class Point
{
public:
    bool operator==(Point& a)
    {
        return x == a.x && y == a.y;
    }
    std::ostream& operator<<(std::ostream& stream)
    {
        stream << "(" << x << ", " << y << ")\n";
        return stream;
    }
    Point& operator= (Point a)
    {
        x = a.x;
        y = a.y;
        index = a.index;
    }
    Point(float a, float b, int i): x(a), y(b), index(i){}
    Point(float a, float b): x(a), y(b){}
    Point(){}
    float x;
    float y;
    int index;
};

class Segment
{
public:
    Segment(Point a, Point b): s_begin(a), s_end(b) {}
    float seg_len()
    {
        return sqrt((s_end.x - s_begin.x) * (s_end.x - s_begin.x) + (s_end.y - s_begin.y) * (s_end.y - s_begin.y));
    }
    float cross_product (Point& a)
    {
        return ((this -> s_end.x - this -> s_begin.x) * (a.y - this->s_begin.y) - (this->s_end.y - this->s_begin.y) * (a.x - this->s_begin.x)) / 2;
    }
    Point s_begin;
    Point s_end;
};

void generate_points(std::vector<Point>& points)
{
    int psize;
    std::cout << "enter amount of points >\n";
    std::cin >> psize;
    points.resize(psize);
    for(auto it = points.begin(); it != points.end(); ++it)
    {
        it -> x = rand() % 1999 + 1;
        it -> y = rand() % 1999 + 1;
        it -> index = it - points.begin() + 1;
    }
}

bool point_belongs_to_hull(std::vector<Point> convexhull, Point& p)
{
    auto it = convexhull.begin();
    for(; it != convexhull.end() && !(*it == p); ++it){}
    return it != convexhull.end();
}

void build_convex_hull (std::vector<Point>& points, std::vector<Point>& convexhull)
{
    sort(points.begin(), points.end(), [](Point a, Point b){if(a.y < b.y) return true; if (a.y == b.y) return a.x > b.x; return false;});
    auto prev_it2 = convexhull.begin();
    for(auto it = points.begin(); it != points.end();)
    {
        convexhull.push_back(*it);
        auto end_it = it;
        for(auto it1 = convexhull.begin(); it1 < convexhull.end() - 2; ++it1)
        {
            Segment seg(*it1, *it);
            for(auto it2 = it1 + 1; it2 < convexhull.end() - 1;)
            {
                if (seg.cross_product(*it2) >= 0)
                {
                    prev_it2 = it2;
                    convexhull.erase(prev_it2);
                }
                else
                {
                    ++it2;
                }
            }
        }
        while(it -> y == end_it -> y && it != points.end())
        {
            ++it;
        }
    }
    if(*(convexhull.end() - 1) == *(points.rbegin()))
    {
        convexhull.erase(convexhull.end() - 1);
    }
    int firstpartsize = convexhull.size();
    for(auto it = points.rbegin(); it != points.rend();)
    {
        convexhull.push_back (*it);
        auto end_it = it;
        for(auto it1 = convexhull.begin() + firstpartsize; it1 < convexhull.end() - 2; ++it1)
        {
            Segment seg(*it1, *it);
            for(auto it2 = it1 + 1; it2 < convexhull.end() - 1;)
            {
                if (seg.cross_product(*it2) >= 0)
                {
                    prev_it2 = it2;
                    convexhull.erase(prev_it2);
                }
                else
                {
                    ++it2;
                }
            }
        }
        while(it -> y == end_it -> y && it != points.rend())
        {
            ++it;
        }
    }
    if(*(convexhull.end() - 1) == *(convexhull.begin()))
    {
        convexhull.erase(convexhull.end() - 1);
    }
}

void find_max_distance (std::vector<Point>& convexhull, int& firsti, int& secondi)
{
    auto it1 = convexhull.begin();
    auto it2 = max_element(convexhull.begin(), convexhull.end(), [](Point a, Point b){if(a.y < b.y) return true; if (a.y == b.y) return a.x > b.x; return false;});
    auto maxdistit1 = it1;
    auto maxdistit2 = it2;
    Segment maxseg(*it1, *it2);
    float maxdist = maxseg.seg_len();
    auto lastit = it2;
    while (it1 < lastit - 1 || it2 < convexhull.end() - 1)
    {
        Point piv((it2 -> x) + ((it1 + 1) -> x) - (it1 -> x), (it2 -> y) + ((it1 + 1) -> y) - (it1 -> y));
        Segment seg(*it2, piv);
        if(seg.cross_product(*(it2 + 1)) <= 0)
        {
            ++it1;
        }
        else
        {
            ++it2;
        }
        Segment newseg(*it1, *it2);
        if(newseg.seg_len() > maxdist)
        {
            maxdistit1 = it1;
            maxdistit2 = it2;
            maxdist = newseg.seg_len();
        }
    }
    firsti = maxdistit1 -> index;
    secondi = maxdistit2 -> index;
    std::cout << maxdist << "\n";
}

void input(std::vector<Point>& points)
{
    int psize;
    float x, y;
    std::cout << "enter amount of points >\n";
    std::cin >> psize;
    for(int i = 0; i < psize; ++i)
    {
        std::cin >> x >> y;
        Point p(x, y, i + 1);
        points.push_back(p);
    }
}

int main()
{
    std::vector<Point> points;
    std::vector<Point> convexhull;
    srand(7);
    std::string tmp_str;
    std::cout << "enter gen to generate array of points, input to enter it manually\n> ";
    std::cin >> tmp_str;
    if(tmp_str == "gen")
    {
        generate_points(points);
    }
    if(tmp_str == "input")
    {
        input(points);
    }
    assert(tmp_str == "gen" || tmp_str == "input");
    build_convex_hull(points, convexhull);
    int firsti, secondi;
    find_max_distance(convexhull, firsti, secondi);
    std::cout << firsti << " " << secondi << "\n";
    points.clear();
    convexhull.clear();
    return 0;
}
