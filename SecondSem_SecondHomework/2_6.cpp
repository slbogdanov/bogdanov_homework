#include <iostream>
#include <stdlib.h>
#include <vector>
#define THRESHOLD 10000
#include <iomanip>
#include <cmath>

class BigInt
{
public:
    BigInt() {sign = 0;}
    BigInt(const int a)
    {
        parts.push_back(a % THRESHOLD);
        parts.push_back((a / THRESHOLD) % THRESHOLD);
        parts.push_back(a / (THRESHOLD * THRESHOLD));
        sign = 0;
    }
    BigInt& operator= (const int a)
    {
        parts.clear();
        parts.push_back(a % THRESHOLD);
        parts.push_back((a / THRESHOLD) % THRESHOLD);
        parts.push_back(a / (THRESHOLD * THRESHOLD));
        sign = 0;
        return *this;
    }
    BigInt operator+ (const BigInt& another_bigint)
    {
        int sum;
        int extra = 0;
        BigInt res_num;
        for(unsigned int i = 0; (i < parts.size()) || (i < another_bigint.parts.size()) || (extra > 0); ++i)
        {
            sum = ((parts[i] * (i < parts.size())) + (another_bigint.parts[i] * (i < another_bigint.parts.size())) + extra);
            res_num.parts.push_back(sum % THRESHOLD);
            extra = sum / THRESHOLD;
        }
        return res_num;
    }
    BigInt& operator= (const BigInt& another_bigint)
    {
        this -> parts.clear();
        this -> sign = another_bigint.sign;
        for(std::vector<int>::const_iterator  it = another_bigint.parts.begin(); it != another_bigint.parts.end(); ++it)
        {
            parts.push_back(*it);
        }
        return *this;
    }
    std::istream& operator>> (std::istream& input)
    {
        std::string mystring;
        int power = 1;
        int number = 0;
        input >> mystring;
        for(signed int i = mystring.size(); i > 0; --i)
        {
            number += (mystring[i - 1] - '0') * power;
            power *= 10;
            if(power == THRESHOLD)
            {
                parts.push_back(number);
                number = 0;
                power = 1;
            }
        }
        if(number != 0 || parts.size() == 0)
        {
            parts.push_back(number);
        }
        return input;
    }
    std::ostream& operator<< (std::ostream& output)
    {
        if(sign == 1)
        {
            output << "-";
        }
        std::vector<int>::iterator it = (--parts.end());
        while((*it) == 0 && it != parts.begin())
        {
            --it;
        }
        output << (*it);
        if(it != parts.begin())
        {
            --it;
            for(; it != parts.begin(); --it)
            {
                output << std::setfill('0') << std::setw(log10(THRESHOLD)) << (*it);
            }
            output << std::setfill('0') << std::setw(log10(THRESHOLD)) << (*parts.begin());
        }
        return output;
    }
    std::vector<int> parts;
    bool sign;
};

void fill_transfer_table(int (&transfer_table)[10][10])
{
    for(int i = 0; i < 10; ++i)
    {
        for(int j = 0; j < 10; ++j)
        {
            transfer_table[i][j] = 0;
        }
    }
    transfer_table[7][6] = 1;
    transfer_table[6][7] = 1;
    transfer_table[7][2] = 1;
    transfer_table[2][7] = 1;
    transfer_table[8][1] = 1;
    transfer_table[1][8] = 1;
    transfer_table[8][3] = 1;
    transfer_table[3][8] = 1;
    transfer_table[9][4] = 1;
    transfer_table[4][9] = 1;
    transfer_table[9][2] = 1;
    transfer_table[2][9] = 1;
    transfer_table[4][0] = 1;
    transfer_table[0][4] = 1;
    transfer_table[4][3] = 1;
    transfer_table[3][4] = 1;
    transfer_table[6][1] = 1;
    transfer_table[1][6] = 1;
    transfer_table[6][0] = 1;
    transfer_table[0][6] = 1;
}

BigInt amount_of_phone_numbers_with_inputted_lenght(int (&transfer_table)[10][10])
{
    BigInt** table_of_phone_numbers_with_current_lenght;
    table_of_phone_numbers_with_current_lenght = new BigInt*[10];
    for(int i = 0; i < 10; ++i)
    {
        if (i == 0 || i == 8)
        {
            table_of_phone_numbers_with_current_lenght[i] = new BigInt(0);
        }
        else
        {
            table_of_phone_numbers_with_current_lenght[i] = new BigInt(1);
        }
    }
    BigInt** table_of_phone_numbers_with_previous_lenght;
    table_of_phone_numbers_with_previous_lenght = new BigInt*[10];
    for(int i = 0; i < 10; ++i)
    {
        table_of_phone_numbers_with_previous_lenght[i] = new BigInt(0);
    }
    BigInt** temppoint;
    int maxlen;
    std::cin >> maxlen;
    for(int len = 2; len <= maxlen; ++len)
    {
        temppoint = table_of_phone_numbers_with_current_lenght;
        table_of_phone_numbers_with_current_lenght = table_of_phone_numbers_with_previous_lenght;
        table_of_phone_numbers_with_previous_lenght = temppoint;
        for(int i = 0; i < 10; ++i)
        {
            *(table_of_phone_numbers_with_current_lenght[i]) = 0;
        }
        for(int i = 0; i < 10; ++i)
        {
            for(int j = 0; j < 10; ++j)
            {
                if(transfer_table[i][j])
                {
                    *(table_of_phone_numbers_with_current_lenght[i]) = (*(table_of_phone_numbers_with_previous_lenght[j]) + *(table_of_phone_numbers_with_current_lenght[i]));
                }
            }
        }
    }
    BigInt* res = new BigInt(0);
    for(int i = 0; i < 10; ++i)
    {
        if(true)
        {
            *res = *(table_of_phone_numbers_with_current_lenght[i]) + *res;
        }
    }
    if (maxlen == 0)
    {
        return 0;
    }
    else
    {
        return (*res);
    }
}

int main()
{
    int transfer_table[10][10];
    fill_transfer_table(transfer_table);
    BigInt res = amount_of_phone_numbers_with_inputted_lenght(transfer_table);
    res << std::cout;
    return 0;
}
