#include <iostream>

template <typename T>
class SharedPtrHelper
{
private:
    int* counter_pointer;
    T* object_pointer;
    void counter_delete()
    {
        if (counter_pointer == nullptr)
            return;
        if (--(*counter_pointer) == 0)
            delete counter_pointer;
    }

public:
    SharedPtrHelper() : counter_pointer(nullptr), object_pointer(nullptr) {}
    SharedPtrHelper(const SharedPtrHelper& sptr_helper) :
        counter_pointer(sptr_helper.counter_pointer), object_pointer(sptr_helper.object_pointer)
    {
        if (counter_pointer != nullptr)
            (*counter_pointer)++;
    }
    SharedPtrHelper(int* counter_ptr, T* pobject) : counter_pointer(counter_ptr), object_pointer(pobject)
    {
        if (counter_pointer != nullptr)
            (*counter_pointer)++;
    }
    void operator() (const SharedPtrHelper& sptr_helper)
    {
        counter_delete();
        counter_pointer  = sptr_helper.counter_pointer;
        object_pointer = sptr_helper.object_pointer;
        (*counter_pointer)++;
    }
    void operator() (int* counter_ptr, T* pobject)
    {
        counter_delete();
        counter_pointer  = counter_ptr;
        object_pointer = pobject;
        if (counter_ptr != nullptr)
            (*counter_pointer)++;
    }
    void empty()
    {
        counter_delete();
        counter_pointer  = nullptr;
        object_pointer = nullptr;
    }
    int* counter_ptr()  const { return counter_pointer; }
    T*   object_ptr() const { return object_pointer; }
    ~SharedPtrHelper() { counter_delete(); }
    bool is_null()
    {
        return object_pointer == nullptr;
    }
};

template <typename T>
class SharedPtr
{
private:
    SharedPtrHelper<T> helper;

public:
    SharedPtr() : helper() {}
    SharedPtr(const SharedPtr& shared_pointer) : helper(shared_pointer.helper.counter_ptr(), shared_pointer.helper.object_ptr()) {}
    SharedPtr(SharedPtr&& shared_pointer) : helper(shared_pointer.helper) { shared_pointer.helper.empty(); }
    SharedPtr(T* object_ptr)
    {
        if (object_ptr == nullptr)
        {
            helper(nullptr, nullptr);
            return;
        }
        int* counter_ptr = new int;
        *counter_ptr = 0;
        helper(counter_ptr, object_ptr);
    }
    SharedPtr& operator= (const SharedPtr& shared_pointer)
    {
        helper(shared_pointer.helper.counter_ptr(), shared_pointer.helper.object_ptr());
        return *this;
    }
    SharedPtr& operator= (SharedPtr&& shared_pointer)
    {
        helper(shared_pointer.helper);
        shared_pointer.helper.empty();
        return *this;
    }
    SharedPtr& operator= (T* object_ptr)
    {
        if (object_ptr == nullptr)
            helper(nullptr, nullptr);
        else
        {
            int* counter_ptr = new int;
            *counter_ptr = 0;
            helper(counter_ptr, object_ptr);
        }
        return *this;
    }
    T* get() const { return helper.object_ptr(); }
    T& operator*()  const { return *get(); }
    T* operator->() const { return get(); }
    int  use_count() const
    {
        if (helper.counter_ptr() == nullptr)
            return 1;
        else
            return *(helper.counter_ptr());
    }
    bool unique()   const { return use_count() == 1; }
    bool is_null()
    {
        return helper.is_null();
    }
};

void f()
{
    int* a = new int;
    std::cin >> *a;
    SharedPtr<int> shared_pointer(a);
    std::cout << "shared pointer's value:";
    std::cout << *shared_pointer << "\n";
    delete a;
}

int main()
{
    int* a = new int;
    std::cin >> *a;
    SharedPtr<int> shared_pointer(a);
    std::cout << "shared pointer's value:";
    std::cout << *shared_pointer << "\n";
    f();
    delete a;
    return 0;
}
