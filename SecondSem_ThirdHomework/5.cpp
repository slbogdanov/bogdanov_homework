#include <iostream>
#include <memory>

template <typename T>
struct Node
{
    std::shared_ptr<Node<T>> right;
    std::shared_ptr<Node<T>> left;
    std::shared_ptr<Node<T>> parent;
    T value;
    Node(){}
    Node(const T& newvalue)
    {
        right = NULL;
        left = NULL;
        parent = NULL;
        value = newvalue;
    }
};

template <typename T>
class BSTree
{
    std::shared_ptr<Node<T>> head;
    int amount_of_nodes;
public:
    BSTree()
    {
        amount_of_nodes = 0;
        head = NULL;
    }
    BSTree(const T& new_value)
    {
        head = std::make_shared<Node<T>>(new_value);
        amount_of_nodes = 1;
    }
    void bst_insert(const T& new_value)
    {
        auto cur_node = head;
        std::shared_ptr<Node<T>> tmp;
        while(true)
        {
            if(new_value >= cur_node -> value)
            {
                if(cur_node -> right == NULL)
                {
                    auto new_node = std::make_shared<Node<T>>(new_value);
                    new_node -> parent = cur_node;
                    cur_node -> right = new_node;
                    if(cur_node -> parent != NULL && cur_node -> parent -> value < cur_node -> value)
                    {
                        cur_node -> parent -> right = std::move(cur_node);
                    }
                    else
                    {
                        if(cur_node -> parent != NULL && cur_node -> parent -> value >= cur_node -> value)
                        {
                            cur_node -> parent -> left = std::move(cur_node);
                        }
                    }
                    break;
                }
                else
                {
                    tmp = std::move(cur_node -> right);
                    if(cur_node -> parent != NULL && cur_node -> parent -> value < cur_node -> value)
                    {
                        cur_node -> parent -> right = std::move(cur_node);
                    }
                    else
                    {
                        if(cur_node -> parent != NULL && cur_node -> parent -> value >= cur_node -> value)
                        {
                            cur_node -> parent -> left = std::move(cur_node);
                        }
                    }
                    cur_node = std::move(tmp);
                }
            }
            else
            {
                if(cur_node -> left == NULL)
                {
                    std::shared_ptr<Node<T>> new_node(new Node<T>(new_value));
                    new_node -> parent = cur_node;
                    cur_node -> left = new_node;
                    if(cur_node -> parent != NULL && cur_node -> parent -> value < cur_node -> value)
                    {
                        cur_node -> parent -> right = std::move(cur_node);
                    }
                    else
                    {
                        if(cur_node -> parent != NULL && cur_node -> parent -> value >= cur_node -> value)
                        {
                            cur_node -> parent -> left = std::move(cur_node);
                        }
                    }
                    break;
                }
                else
                {
                    tmp = std::move(cur_node -> left);
                    if(cur_node -> parent != NULL && cur_node -> parent -> value < cur_node -> value)
                    {
                        cur_node -> parent -> right = std::move(cur_node);
                    }
                    else
                    {
                        if(cur_node -> parent != NULL && cur_node -> parent -> value >= cur_node -> value)
                        {
                            cur_node -> parent -> left = std::move(cur_node);
                        }
                    }
                    cur_node = std::move(tmp);
                }
            }
        }
    }
    std::shared_ptr<Node<T>> find_in_bst(T required_value)
    {
        auto cur_node = head;
        auto last_node_with_bigger_value = head;
        last_node_with_bigger_value = NULL;
        while(true)
        {
            if(cur_node -> value < required_value)
            {
                if(cur_node -> right == NULL)
                {
                    return last_node_with_bigger_value;
                }
                else
                {
                    cur_node = cur_node -> right;
                }
            }
            if(cur_node -> value == required_value)
            {
                return cur_node;
            }
            if(cur_node -> value > required_value)
            {
                last_node_with_bigger_value = cur_node;
                if(cur_node -> left == NULL)
                {
                    return cur_node;
                }
                else
                {
                    cur_node = cur_node -> left;
                }
            }
        }
    }
};


int main()
{
    int value;
    std::cout << "In this program values are considered positive integers.\n";
    std::cout << "enter root's value\n";
    std::cin >> value;
    BSTree<int> bst(value);
    std::cout << "enter another node's value, enter -1 to enter required value next\n";
    std::cin >> value;
    while(value > -1)
    {
        bst.bst_insert(value);
        std::cout << "enter another node's value, enter -1 to enter required value next\n";
        std::cin >> value;
    }
    std::cout << "now enter required value\n";
    std::cin >> value;
    std::cout << "closest greater value in BST is: ";
    if(bst.find_in_bst(value) == NULL)
    {
        std::cout << "there is no greater value in BST\n";
    }
    else
    {
        std::cout << bst.find_in_bst(value) -> value << "\n";
    }
    return 0;
}
