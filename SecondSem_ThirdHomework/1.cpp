#include <algorithm>
#include <iostream>
#include <array>
#include <cstdlib>
#include <random>
#include <assert.h>
#define AMOUNTOFGENNUM 1000
#define RANGEOFGENNUM 100

void print_vect(std::vector<int> tmpvect)
{
    for(auto it = tmpvect.begin(); it != tmpvect.end(); ++it)
    {
        std::cout << *it << " ";
    }
    std::cout << "\n";
}

void part1(std::vector<int>& tmpvect)
{
    tmpvect.resize(AMOUNTOFGENNUM);
    std::generate(tmpvect.begin(), tmpvect.end(), [](){return (std::rand()%RANGEOFGENNUM);});
    std::cout << "generated vector:\n";
    print_vect(tmpvect);
}

void part2(std::vector<int>& tmpvect, std::vector<int>& tmpvectcopy)
{
    std::copy(tmpvect.begin(), tmpvect.end(), back_inserter(tmpvectcopy));
    std::cout << "copied vector:\n";
    print_vect(tmpvectcopy);
}

void part3(std::vector<int>& tmpvectcopy)
{
    std::shuffle(tmpvectcopy.begin(), tmpvectcopy.end(), std::default_random_engine(0));
    std::cout << "shuffled vector:\n";
    print_vect(tmpvectcopy);
}

void part4(std::vector<int>& tmpvectcopy)
{
    std::sort(tmpvectcopy.begin(), tmpvectcopy.end(), [](int a, int b){return a > b;});
    std::cout << "sorted vector:\n";
    print_vect(tmpvectcopy);
}

void part5(std::vector<int>& tmpvectcopy)
{
    std::reverse(tmpvectcopy.begin(), tmpvectcopy.end());
    std::cout << "reversed vector:\n";
    print_vect(tmpvectcopy);
}

void part6(std::vector<int>& tmpvect, std::vector<int>& tmpvectcopy)
{
    assert(*(tmpvectcopy.begin()) == *min_element(tmpvect.begin(), tmpvect.end(), [](int a, int b){return a < b;}));
}

void part7(std::vector<int>& tmpvect, std::vector<int>& tmpvectcopy)
{
    assert(*(tmpvectcopy.end() - 1) == *max_element(tmpvect.begin(), tmpvect.end(), [](int a, int b){return a < b;}));
}

void part8(std::vector<int>& permutation)
{
    permutation.resize(AMOUNTOFGENNUM);
    std::generate(permutation.begin(), permutation.end(), [](){static int i = -1; ++i; return i;});
    std::cout << "permutation:\n";
    print_vect(permutation);
}

void part9(std::vector<int>& tmpvect, std::vector<int>& permutation)
{
    std::sort(permutation.begin(), permutation.end(), [&tmpvect](int a, int b){return tmpvect[a] < tmpvect[b];});
    std::cout << "displacements to sort array:\n";
    print_vect(permutation);
}

void part10(std::vector<int>& sorted, std::vector<int>& tmpvect, std::vector<int>& permutation)
{
    std::transform(permutation.begin(), permutation.end(), back_inserter(sorted), [&tmpvect](int i){return tmpvect[i];});
    std::cout << "transformed array:\n";
    print_vect(sorted);
}

void part11(std::vector<int>& sorted)
{
    assert(is_sorted(sorted.begin(), sorted.end(), [](int a, int b){return a < b;}));
}

void part12(std::vector<int>& sorted, std::vector<int>& tmpvectcopy)
{
    assert(std::equal(sorted.begin(), sorted.end(), tmpvectcopy.begin()));
}

void part13(std::vector<int>& counts, std::vector<int>& tmpvect)
{
    counts.resize(RANGEOFGENNUM);
    std::generate(counts.begin(), counts.end(), [&tmpvect](){static int i = -1; ++i; return std::count(tmpvect.begin(), tmpvect.end(), i);});
    std::cout << "counts:\n";
    print_vect(counts);
}

void part14(std::vector<int>& counts, std::vector<int>& tmpvect, std::vector<int>& sorted)
{
    counts.resize(RANGEOFGENNUM);
    std::generate(counts.begin(), counts.end(), [&tmpvect, &sorted](){static int i = -1;
                                                                      ++i;
                                                                      int counter = std::count(tmpvect.begin(), tmpvect.end(), i);
                                                                      assert(counter = std::upper_bound(sorted.begin(), sorted.end(), i) - std::lower_bound(sorted.begin(), sorted.end(), i));
                                                                      return counter;});
}

void part15(std::vector<int>& counts)
{
    int sum = 0;
    std::for_each(counts.begin(), counts.end(), [&sum](int i){sum += i;});
    assert(sum = AMOUNTOFGENNUM);
    std::cout << "sum of all elements in counts:" << sum << "\n";
}

void part16(std::vector<int>& sorted)
{
    std::cout << "sorted elements of array:\n";
    print_vect(sorted);
}

int main()
{
    std::vector<int> tmpvect;
    part1(tmpvect);
    std::vector<int> tmpvectcopy;
    part2(tmpvect, tmpvectcopy);
    part3(tmpvectcopy);
    part4(tmpvectcopy);
    part5(tmpvectcopy);
    part6(tmpvect, tmpvectcopy);
    part7(tmpvect, tmpvectcopy);
    std::vector<int> permutation;
    part8(permutation);
    part9(tmpvect, permutation);
    std::vector<int> sorted;
    part10(sorted, tmpvect, permutation);
    part11(sorted);
    part12(sorted, tmpvectcopy);
    std::vector<int> counts;
    part13(counts, tmpvect);
    part14(counts, tmpvect, sorted);
    part15(counts);
    part16(sorted);
    return 0;
}
