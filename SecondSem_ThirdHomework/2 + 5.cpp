#include <iostream>
#include <assert.h>

template <typename T>
class SharedPtrHelper
{
private:
    int* counter_pointer;
    T* object_pointer;
    void counter_delete()
    {
        if (counter_pointer == nullptr)
            return;
        if (--(*counter_pointer) == 0)
            delete counter_pointer;
    }

public:
    SharedPtrHelper() : counter_pointer(nullptr), object_pointer(nullptr) {}
    SharedPtrHelper(const SharedPtrHelper& sptr_helper) :
        counter_pointer(sptr_helper.counter_pointer), object_pointer(sptr_helper.object_pointer)
    {
        if (counter_pointer != nullptr)
            (*counter_pointer)++;
    }
    SharedPtrHelper(int* counter_ptr, T* pobject) : counter_pointer(counter_ptr), object_pointer(pobject)
    {
        if (counter_pointer != nullptr)
            (*counter_pointer)++;
    }
    void operator() (const SharedPtrHelper& sptr_helper)
    {
        counter_delete();
        counter_pointer  = sptr_helper.counter_pointer;
        object_pointer = sptr_helper.object_pointer;
        (*counter_pointer)++;
    }
    void operator() (int* counter_ptr, T* pobject)
    {
        counter_delete();
        counter_pointer  = counter_ptr;
        object_pointer = pobject;
        if (counter_ptr != nullptr)
            (*counter_pointer)++;
    }
    void empty()
    {
        counter_delete();
        counter_pointer  = nullptr;
        object_pointer = nullptr;
    }
    int* counter_ptr()  const { return counter_pointer; }
    T*   object_ptr() const { return object_pointer; }
    ~SharedPtrHelper() { counter_delete(); }
    bool is_null()
    {
        return object_pointer == nullptr;
    }
};

template <typename T>
class SharedPtr
{
private:
    SharedPtrHelper<T> helper;

public:
    SharedPtr() : helper() {}
    SharedPtr(const SharedPtr& shared_pointer) : helper(shared_pointer.helper.counter_ptr(), shared_pointer.helper.object_ptr()) {}
    SharedPtr(SharedPtr&& shared_pointer) : helper(shared_pointer.helper) { shared_pointer.helper.empty(); }
    SharedPtr(T* object_ptr)
    {
        if (object_ptr == nullptr)
        {
            helper(nullptr, nullptr);
            return;
        }
        int* counter_ptr = new int;
        *counter_ptr = 0;
        helper(counter_ptr, object_ptr);
    }
    SharedPtr& operator= (const SharedPtr& shared_pointer)
    {
        helper(shared_pointer.helper.counter_ptr(), shared_pointer.helper.object_ptr());
        return *this;
    }
    SharedPtr& operator= (SharedPtr&& shared_pointer)
    {
        helper(shared_pointer.helper);
        shared_pointer.helper.empty();
        return *this;
    }
    SharedPtr& operator= (T* object_ptr)
    {
        if (object_ptr == nullptr)
            helper(nullptr, nullptr);
        else
        {
            int* counter_ptr = new int;
            *counter_ptr = 0;
            helper(counter_ptr, object_ptr);
        }
        return *this;
    }
    T* get() const { return helper.object_ptr(); }
    T& operator*()  const { return *get(); }
    T* operator->() const { return get(); }
    int  use_count() const
    {
        if (helper.counter_ptr() == nullptr)
            return 1;
        else
            return *(helper.counter_ptr());
    }
    bool unique()   const { return use_count() == 1; }
    bool is_null()
    {
        return helper.is_null();
    }
};

template <typename T>
struct Node
{
    SharedPtr<Node<T>> right;
    SharedPtr<Node<T>> left;
    SharedPtr<Node<T>> parent;
    T value;
    Node(){}
    Node(const T& newvalue)
    {
        right = nullptr;
        left = nullptr;
        parent = nullptr;
        value = newvalue;
    }
};

template <typename T>
class BSTree
{
    SharedPtr<Node<T>> head;
    int amount_of_nodes;
public:
    BSTree()
    {
        amount_of_nodes = 0;
        head = nullptr;
    }
    BSTree(const T& new_value)
    {
        SharedPtr<Node<T>> new_node(new Node<T>(new_value));
        head = new_node;
        amount_of_nodes = 1;
    }
    void bst_insert(const T& new_value)
    {
        auto cur_node = head;
        amount_of_nodes++;
        SharedPtr<Node<T>> new_node;
        while(true)
        {
            assert(new_value != cur_node -> value);
            if(new_value < (cur_node -> value))
            {
                if(cur_node -> left.is_null())
                {
                    SharedPtr<Node<T>> new_node(new Node<T>(new_value));
                    new_node -> parent = cur_node;
                    cur_node -> left = std::move(new_node);
                    break;
                }
                else
                {
                    cur_node = cur_node -> left;
                }
            }
            if(new_value > (cur_node -> value))
            {
                if(cur_node -> right.is_null())
                {
                    SharedPtr<Node<T>> new_node(new Node<T>(new_value));
                    new_node -> parent = cur_node;
                    cur_node -> right = std::move(new_node);
                    break;
                }
                else
                {
                    cur_node = cur_node -> right;
                }
            }
        }
    }
    SharedPtr<Node<T>> find_in_bst(T required_value)
    {
        SharedPtr<Node<T>> cur_node;
        cur_node = head;
        SharedPtr<Node<T>> last_node_with_bigger_value;
        last_node_with_bigger_value = nullptr;
        while(true)
        {
            if(cur_node -> value < required_value)
            {
                if(cur_node -> right.is_null())
                {
                    return last_node_with_bigger_value;
                }
                else
                {
                    cur_node = cur_node -> right;
                }
            }
            if(cur_node -> value == required_value)
            {
                return cur_node;
            }
            if(cur_node -> value > required_value)
            {
                last_node_with_bigger_value = cur_node;
                if(cur_node -> left.is_null())
                {
                    return cur_node;
                }
                else
                {
                    cur_node = cur_node -> left;
                }
            }
        }
    }
};

void launch()
{
    int value;
    std::cout << "In this program values are considered positive integers.\n";
    std::cout << "enter root's value\n";
    std::cin >> value;
    BSTree<int> bst(value);
    std::cout << "enter another node's value, enter -1 to enter required value next\n";
    std::cin >> value;
    int value_copy;
    while(value >= 0)
    {
        value_copy = value;
        bst.bst_insert(value_copy);
        std::cout << "enter another node's value, enter -1 to enter required value next\n";
        std::cin >> value;
    }
    std::cout << "now enter required value\n";
    std::cin >> value;
    std::cout << "closest greater value in BST is: ";
    SharedPtr<Node<int>> result;
    result = bst.find_in_bst(value);
    if(result.is_null())
    {
        std::cout << "there is no greater value in BST\n";
    }
    else
    {
        std::cout << result -> value << "\n";
    }
}

int main()
{
    launch();
    return 0;
}
