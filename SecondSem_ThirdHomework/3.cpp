#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <assert.h>
#include <stdlib.h>

class ProgContest
{
public:
    class Task
    {
    public:
        int index;
        int amount_of_solutions;
        Task(int ind, int am): index(ind), amount_of_solutions(am){}
        Task(){}
    };
    class Student
    {
    public:
        std::string name;
        int amount_of_solved_tasks;
        int amount_of_balls;
    };
    struct CompByBalls
    {
        bool operator()(Student* student1, Student* student2) const
        {
            if(student1 -> amount_of_balls != student2 -> amount_of_balls)
            {
                return student1 -> amount_of_balls < student2 -> amount_of_balls;
            }
            else
            {
                return student1 -> name < student2 -> name;
            }
        }
    };
    struct CompBySolvedTasks
    {
        bool operator()(Student* student1, Student* student2) const
        {
            if(student1 -> amount_of_solved_tasks != student2 -> amount_of_solved_tasks)
            {
                return student1 -> amount_of_solved_tasks < student2 -> amount_of_solved_tasks;
            }
            else
            {
                return student1 -> name < student2 -> name;
            }
        }
    };
    struct CompTasks
    {
        bool operator()(Task* task1, Task* task2) const
        {
            if(task1 -> amount_of_solutions != task2 -> amount_of_solutions)
            {
                return task1 -> amount_of_solutions < task2 -> amount_of_solutions;
            }
            else
            {
                return task1 -> index < task2 -> index;
            }
        }
    };
    struct CompByName
    {
        bool operator()(Student* student1, Student* student2) const
        {
            return student1 -> name < student2 -> name;
        }
    };
    void parsing()
    {
        int tmp;
        std::cin >> amount_of_tasks;
        for(int i = 0; i < amount_of_tasks; ++i)
        {
            std::cin >> tmp;
            tasks_by_solutionts.insert(new Task(i, 0));
            balls_for_task.push_back(std::pair<int, int>(tmp, 0));
        }
        std::string tmpstr;
        std::string another_tmpstr;
        Student* tmp_stud = new Student;
        Task* tmp_task = new Task;
        Student* new_stud;
        Task* new_task;
        std::cin >> tmpstr;
        while(tmpstr != "EXIT")
        {
            if(tmpstr == "SOLVED")
            {
                std::cin >> another_tmpstr;
                std::cin >> tmp;

                tmp_task -> index = tmp - 1;
                tmp_task -> amount_of_solutions = balls_for_task[tmp - 1].second;
                auto res_task_it = tasks_by_solutionts.find(tmp_task);
                new_task = *res_task_it;
                tasks_by_solutionts.erase(res_task_it);
                new_task -> amount_of_solutions += 1;
                tasks_by_solutionts.insert(new_task);
                balls_for_task[tmp - 1].second += 1;
                tmp_stud -> name = another_tmpstr;
                auto res_stud_it = students_by_name.find(tmp_stud);
                if(res_stud_it == students_by_name.end())
                {
                    new_stud = new Student;
                    new_stud -> name = another_tmpstr;
                    new_stud -> amount_of_solved_tasks = 1;
                    new_stud -> amount_of_balls = balls_for_task[tmp - 1].first;
                    students_by_name.insert(new_stud);
                    students_by_solved_tasks.insert(new_stud);
                    students_by_balls.insert(new_stud);
                    new_stud = NULL;
                }
                else
                {
                    students_by_balls.erase(students_by_balls.find((*res_stud_it)));
                    students_by_solved_tasks.erase(students_by_solved_tasks.find((*res_stud_it)));
                    (*res_stud_it) -> amount_of_solved_tasks += 1;
                    (*res_stud_it) -> amount_of_balls += balls_for_task[tmp - 1].first;
                    students_by_balls.insert((*res_stud_it));
                    students_by_solved_tasks.insert((*res_stud_it));
                }
            }
            if(tmpstr == "STUD_STAT")
            {
                std::cin >> another_tmpstr;
                tmp_stud -> name = another_tmpstr;
                auto res_stud_it = students_by_name.find(tmp_stud);
                assert(res_stud_it != students_by_name.end());
                std::cout << (*res_stud_it) -> amount_of_solved_tasks << " " << (*res_stud_it) -> amount_of_balls << "\n";
            }
            if(tmpstr == "STUDS_BY_TASKS")
            {
                for(auto it = students_by_solved_tasks.begin(); it != students_by_solved_tasks.end(); ++it)
                {
                    std::cout << (*it) -> name << " " << (*it) -> amount_of_solved_tasks << "\n";
                }
            }
            if(tmpstr == "STUDS_BY_BALLS")
            {
                for(auto it = students_by_balls.begin(); it != students_by_balls.end(); ++it)
                {
                    std::cout << (*it) -> name << " " << (*it) -> amount_of_balls << "\n";
                }
            }
            if(tmpstr == "STUDS_MORE_TASKS")
            {
                std::cin >> tmp;
                for(auto it = students_by_solved_tasks.begin(); it != students_by_solved_tasks.end(); ++it)
                {
                    if((*it) -> amount_of_solved_tasks > tmp)
                    std::cout << (*it) -> name << " " << (*it) -> amount_of_solved_tasks << "\n";
                }
            }
            if(tmpstr == "STUDS_MORE_BALLS")
            {
                std::cin >> tmp;
                for(auto it = students_by_balls.begin(); it != students_by_balls.end(); ++it)
                {
                    if((*it) -> amount_of_balls > tmp)
                    std::cout << (*it) -> name << " " << (*it) -> amount_of_balls << "\n";
                }
            }
            if(tmpstr == "TASKS_BY_SOLUTIONS")
            {
                for(auto it = tasks_by_solutionts.begin(); it != tasks_by_solutionts.end(); ++it)
                {
                    std::cout << (*it) -> index + 1 << " " << (*it) -> amount_of_solutions << "\n";
                }
            }
            std::cin >> tmpstr;
        }
        delete tmp_stud;
        delete tmp_task;
    }
    ProgContest(){}
    ~ProgContest()
    {
        for(auto it = students_by_name.begin(); it != students_by_name.end(); ++it)
        {
            delete (*it);
        }
        for(auto it = tasks_by_solutionts.begin(); it != tasks_by_solutionts.end(); ++it)
        {
            delete (*it);
        }
    }
private:
    int amount_of_tasks;
    std::vector<std::pair<int, int>> balls_for_task;
    std::set<Task*, CompTasks> tasks_by_solutionts;
    std::set<Student*, CompByBalls> students_by_balls;
    std::set<Student*, CompBySolvedTasks> students_by_solved_tasks;
    std::set<Student*, CompByName> students_by_name;
};

int main()
{
    ProgContest prog_contest;
    prog_contest.parsing();
    return 0;
}
