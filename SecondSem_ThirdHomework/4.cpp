#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <assert.h>
#include <stdlib.h>
int new_counter = 0;
int delete_counter = 0;

class BeautyContest
{
public:
    class Voter
    {
    public:
        std::string phone_number;
        bool is_vip;
        std::vector<std::string> votes;
        Voter()
        {is_vip = 0;}
    };
    class Participant
    {
    public:
        std::string name;
        int amount_of_votes;
    };
    struct CompByPhoneNumber
    {
        bool operator()(Voter* voter1, Voter* voter2) const
        {
            return voter1 -> phone_number < voter2 -> phone_number;
        }
    };
    struct CompByVotes
    {
        bool operator()(Participant* participant1, Participant* participant2) const
        {
            if(participant1 -> amount_of_votes != participant2 -> amount_of_votes)
            {
                return participant1 -> amount_of_votes < participant2 -> amount_of_votes;
            }
            else
            {
                return participant1 -> name > participant2 -> name;
            }
        }
    };
    struct CompByName
    {
        bool operator()(Participant* participant1, Participant* participant2) const
        {
            return participant1 -> name < participant2 -> name;
        }
    };
    void parsing()
    {
        std::string tmp_str;
        std::string another_tmp_str;
        int tmp;
        Participant* tmp_part = new Participant;
        new_counter ++;
        Voter* tmp_voter = new Voter;
        new_counter ++;
        Participant* new_part;
        Voter* new_voter;
        while(tmp_str != "EXIT")
        {
            if(tmp_str == "REGISTER")
            {
                std::cin >> another_tmp_str;
                tmp_part -> name = another_tmp_str;
                auto res_part_it = participants_by_name.find(tmp_part);
                if(res_part_it != participants_by_name.end())
                {
                    std::cout << "ALREADY REGISTERED\n";
                }
                else
                {
                    new_part = new Participant;
                    new_counter ++;
                    new_part -> name = another_tmp_str;
                    new_part -> amount_of_votes = 0;
                    participants_by_name.insert(new_part);
                    participants_by_votes.insert(new_part);
                    new_part = NULL;
                    std::cout << "OK\n";
                }
            }
            if(tmp_str == "VOTE")
            {
                std::cin >> another_tmp_str;
                std::cin >> tmp_str;
                tmp_part -> name = another_tmp_str;
                auto res_part_it = participants_by_name.find(tmp_part);
                if(res_part_it == participants_by_name.end())
                {
                    std::cout << "NOT REGISTERED\n";
                }
                else
                {
                    tmp_voter -> phone_number = tmp_str;
                    auto res_voter_it = voters_by_phone_number.find(tmp_voter);
                    if(res_voter_it == voters_by_phone_number.end())
                    {
                        new_voter = new Voter;
                        new_counter ++;
                        new_voter -> phone_number = tmp_str;
                        new_voter -> votes.push_back(another_tmp_str);
                        voters_by_phone_number.insert(new_voter);
                        new_voter = NULL;
                        res_voter_it = voters_by_phone_number.find(tmp_voter);
                    }
                    if((*res_voter_it) -> votes.size() > (1 + ((*res_voter_it) -> is_vip) * 3))
                    {
                        std::cout << "NO MORE VOTES\n";
                    }
                    else
                    {
                        participants_by_votes.erase(participants_by_votes.find(*res_part_it));
                        (*res_part_it) -> amount_of_votes += 1;
                        participants_by_votes.insert(*res_part_it);
                        ((*res_voter_it) -> votes).push_back(another_tmp_str);
                        std::cout << "OK\n";
                    }
                }
            }
            if(tmp_str == "VIP")
            {
                std::cin >> another_tmp_str;
                tmp_voter -> phone_number = another_tmp_str;
                auto res_voter_it = voters_by_phone_number.find(tmp_voter);
                if(res_voter_it == voters_by_phone_number.end())
                {
                    new_voter = new Voter;
                    new_counter ++;
                    new_voter -> phone_number = tmp_str;
                    new_voter -> is_vip = 1;
                    voters_by_phone_number.insert(new_voter);
                    new_voter = NULL;
                    res_voter_it = voters_by_phone_number.find(tmp_voter);
                }
                if((*res_voter_it) -> is_vip)
                {
                    std::cout << "ALREADY VIP\n";
                }
                else
                {
                    (*res_voter_it) -> is_vip = 1;
                    std::cout << "OK\n";
                }
            }
            if(tmp_str == "TOP")
            {
                std::cin >> tmp;
                int counter = 0;
                for(auto it = participants_by_votes.rbegin(); it != participants_by_votes.rend() && counter < tmp; ++it, ++counter)
                {
                    std::cout << (*it) -> name << " " << (*it) -> amount_of_votes << "\n";
                }
            }
            if(tmp_str == "KICK")
            {
                std::cin >> another_tmp_str;
                tmp_part -> name = another_tmp_str;
                auto res_part_it = participants_by_name.find(tmp_part);
                if(res_part_it == participants_by_name.end())
                {
                    std::cout << "NOT REGISTERED\n";
                }
                else
                {
                    participants_by_votes.erase(participants_by_votes.find(*res_part_it));
                    participants_by_name.erase(*res_part_it);
                    delete (*res_part_it);
                    for(auto it = voters_by_phone_number.begin(); it != voters_by_phone_number.end(); ++it)
                    {
                        auto res_voter_it = (*it) -> votes.begin();
                        while((*res_voter_it) != another_tmp_str && res_voter_it !=  (*it) -> votes.end())
                        {
                            res_voter_it ++;
                        }
                        if(res_voter_it != (*it) -> votes.end())
                        {
                            (*it) -> votes.erase(res_voter_it);
                        }
                        if((*it) -> votes.size() == 0)
                        {
                            auto tmp_it = it;
                            voters_by_phone_number.erase(tmp_it);
                            delete (*tmp_it);
                            delete_counter ++;
                        }
                    }
                    std::cout << "OK\n";
                }
            }
            std::cin >> tmp_str;
        }
        delete tmp_part;
        delete_counter ++;
        delete tmp_voter;
        delete_counter ++;
    }
    BeautyContest(){}
    ~BeautyContest()
    {
        for(auto it = participants_by_name.begin(); it != participants_by_name.end(); ++it)
        {
            delete (*it);
            delete_counter ++;
        }
        for(auto it = voters_by_phone_number.begin(); it != voters_by_phone_number.end(); ++it)
        {
            delete (*it);
            delete_counter ++;
        }
    }
private:
    std::set<Participant*, CompByVotes> participants_by_votes;
    std::set<Participant*, CompByName> participants_by_name;
    std::set<Voter*, CompByPhoneNumber> voters_by_phone_number;
};

void tmpf()
{
    BeautyContest beauty_contest;
    beauty_contest.parsing();
}

int main()
{
    tmpf();
    std::cout << new_counter << " " << delete_counter << "\n";
    return 0;
}
